# Script en python que agafa el zip file que se li passa per paràmetre i extreu i descomprimeix el fitxer vgz a vgm perquè sigui reproduït pel roboplay de MSX.
# Busca el fitxer txt explicatiu del xip i si és OPLL el transforma, altrament només descomprimeix i canvia el nom.

import zipfile
import gzip
import random
import subprocess
import os
import shutil

def descomprimir_vgz(compressed_file, destination_file):
    # Open the compressed file in binary read mode
    with gzip.open(compressed_file, 'rb') as infile:
        # Open the destination file in binary write mode
        with open(destination_file, 'wb') as outfile:
            # Read and write the decompressed data in chunks for efficiency
            for chunk in iter(lambda: infile.read(1024), b''):
                outfile.write(chunk)

    # Print success message
    print(f"Decompressed {compressed_file} to {destination_file}")

def mirarNomExisteix(arrayNoms, nomFitxer):
    # Mirarem si el nom ja ha estat posat en aquest procés
    nomFitxerSenseEspais = nomFitxer.replace(' ','').replace('_','').replace('-','').replace('(','').replace(')','').replace('"',"").replace("'","").replace("[","").replace("]","")
    nomCandidat = nomFitxerSenseEspais[:8]
    candidatValid = False
    comptadorCaracter = 8
    while not candidatValid:
        if nomCandidat not in arrayNoms:
            candidatValid = True
        else:
            if (len(nomFitxerSenseEspais)-4)>=candidatValid : # Resto 4 de l'extensió, no vull utilitzar aquests caràcters
                nomCandidat[7] = nomFitxerSenseEspais[comptadorCaracter]
            else:
                nomCandidat[7] = char(random.randint(1,128))
        comptadorCaracter += 1

    return(nomCandidat)

nomZipTot = '/mnt/DATA/Videojocs/MSX/Musica_VGM/Sorcerian_(MSX2,_OPLL).zip'
nomZipPath, nomZipFitxer = os.path.split(nomZipTot)

zip_file = zipfile.ZipFile(nomZipTot,'r')

try:
    os.mkdir(nomZipFitxer[:8])
except FileExistsError:
    print(f"Directori {nomZipFitxer[:8]} ja creat")

esOPLL = False
nomsUsats = []

# Descobrim per quin tipus de xip és
for file in zip_file.infolist():
    if file.filename.find('txt')>0:
        with zip_file.open(file.filename) as txt:
            if txt.read().decode('utf-8').find('YM2413')>=0:
                esOPLL = True
                print("Es OPLL")

# Els anem descomprimint
for file in zip_file.infolist():
    # L'he d'extreure del zipfile
    zip_file.extract(file, '.')
    nomfitx = mirarNomExisteix(nomsUsats, file.filename)
    nomFinal = nomZipFitxer[:8] + '/' + nomfitx + ".vgm"
    nomsUsats.append(nomFinal)
    if file.filename.find('vgz')>0:
        descomprimir_vgz(file.filename, nomFinal)
    elif file.filename.find('vgm')>0:
        shutil.copy(file.filename, nomFinal)
    if esOPLL:
        # Utilitza vgm_conv per passar-lo a OPL2
        subprocess.run(["/home/jepsuse/MSX/VGM/vgm-conv/node_modules/.bin/vgm-conv","-f","ym2413","-t","ym3812","-i",nomFinal,"-o",nomFinal])
    os.remove(file.filename)
print(f"Extret fitxer {file.filename}")

