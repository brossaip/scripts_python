# Fitxer que utilitza les comandes de l'openmsx (com el reprodueixCançonsMoonblaster.py) per escombrar per tots els fitxers que tinc descarregats de la targeta Compact Flash i mirar si sonen o no amb l'openMSX.
# Si sonen les guardem en el fitxer per fer el m3u. Que no, ho escrivim el fitxer NoSonen.txt
# L'openmsx ha d'estar corrent com: openmsx -machine Panasonic_FS-A1ST -ext moonsound -diska ~/MSX/roboplay/dsk/
# !!!!! Hauria de poder detectar quan el MSX s'ha quedat penjat per fer que les cançons no sonin !!!!
import time
import socket
import glob
import shutil
import os
import wave
import numpy as np
from PIL import Image
import subprocess

def calcula_dif_imatges(image1_path, image2_path):
  """
  Calculates the Mean Squared Error (MSE) distance between two images.

  Args:
      image1_path: Path to the first image file.
      image2_path: Path to the second image file.

  Returns:
      The MSE distance between the images.
  """
  try:
    image1 = Image.open(image1_path)
    image2 = Image.open(image2_path)

    # Ensure images have the same dimensions and mode
    if image1.size != image2.size or image1.mode != image2.mode:
      raise ValueError("Images must have the same size and mode")

    # Convert to grayscale if needed (ignores alpha channel)
    if image1.mode != 'L':
      image1 = image1.convert('L')
      image2 = image2.convert('L')

    # Calculate pixel-wise difference
    diff = np.sum((np.array(image1) - np.array(image2))**2)

    return diff

  except (IOError, OSError) as e:
    print(f"Error opening images: {e}")
    return 1000 # Retorna 1000 perquè no es pengi el programa i no afecti a si ha sonat o no

def start_and_kill_process(command):
  """
  Starts a process using subprocess, returns its PID, and provides a way to kill it.

  Args:
      command: The command (list of arguments) to execute as a subprocess.

  Returns:
      A tuple containing the PID of the subprocess and a function to kill it.
  """
  process = subprocess.Popen(command)
  def kill_process():
    process.terminate()
    print(f"Process with PID {process.pid} terminated gracefully")
  return process.pid, kill_process

# Per trobar el pid fem pidof openmsx. També podem fer un ls de /tmp/openmsx-jepsuse/socket.
pid, func_kill_openMSX = start_and_kill_process(['/home/jepsuse/MSX/openMSX/derived/openmsx','-machine','Panasonic_FS-A1ST','-ext','moonsound', '-diska', '/home/jepsuse/MSX/roboplay/dsk/'])
pid_openMSX = str(pid)
time.sleep(5)

fitxer_ok = open("okRoboplay.m3u","a")
fitxer_ko = open("NoSonenRoboplay.txt","a")

# fitxers = glob.glob('/var/run/media/jepsuse/PRINCIPAL/WozBlast/OPL4/TotMusic/*') # No va fer subdirectoris perquè faltava el **. He creat un fitxer amb el find i és el que executaré
llistatfitxers = open("LlistatOPL4.txt","r")
fitxers = llistatfitxers.readlines()

numPorto = 1;
start_time = time.time()

for fitxer in fitxers[3000:]:
  fitxer = fitxer.strip()
  loop_start = time.time()
  if os.path.exists(fitxer):
    if os.path.getsize(fitxer.strip())<16*1024:
      if fitxer.upper().find('.MWK')<0 and fitxer.find('.')>=0:
        s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        s.connect('/tmp/openmsx-jepsuse/socket.' + pid_openMSX) # A la canço 20 es penjava. Vaig prova de reconnectar-ho a cada moment
        print(f"Cançó {fitxer}. Número {numPorto} de {len(fitxers)}")

        # A cops es penja per la cançó, farem un reset
        s.send(bytes('<command>reset</command>',"utf-8"))
        s.send(bytes("<command>set fastforward on</command>","utf-8"))
        time.sleep(5)
        s.send(bytes("<command>set fastforward off</command>","utf-8"))

        [path, nomfitx]=os.path.split(fitxer)
        pos_TotMusic = fitxer.find('TotMusic')
        pos_Fitx = fitxer.find(nomfitx)
        cami_msx = fitxer[pos_TotMusic + 8:pos_Fitx]
        os.makedirs('/home/jepsuse/MSX/roboplay/dsk/WozBlast/OPL4/TotMusic/' + cami_msx, exist_ok=True)
            
        fitxmbms = glob.glob(fitxer[0:-3]+'*')
        for fitxmbm in fitxmbms:
          try:
            shutil.copy(fitxmbm, '/home/jepsuse/MSX/roboplay/dsk/WozBlast/OPL4/TotMusic/' + cami_msx + '/')
          except:
            print(f"$$$ No hem pogut copiar fitxer {fitxmbm}")

        s.send(bytes('<command>type "roboplay " </command>',"utf-8"))
        time.sleep(1)
        # Hauré de fer split i separar per / fent un keymatrixdown 1 64!!!
        print(f"!!! cami msx {cami_msx}")
        if cami_msx != '/':
          cami_enviar_type = 'WozBlast/OPL4/TotMusic/' + cami_msx
        else:
          cami_enviar_type = 'WozBlast/OPL4/TotMusic/'
          separat = cami_enviar_type.split('/')
          for element in separat:
            if len(element)>0:
              s.send(bytes("<command>type " + element + "</command>","utf-8"))
              time.sleep(0.3 * len(element))
              s.send(bytes("<command>keymatrixdown 1 16</command>","utf-8")) # caràcter \
              time.sleep(0.3)
              s.send(bytes("<command>keymatrixup 1 16</command>","utf-8"))
              time.sleep(0.3)
          s.send(bytes("<command>type " + nomfitx + "</command>","utf-8"))
          time.sleep(0.3*len(nomfitx))
          s.send(bytes("<command>keymatrixdown 7 128</command>","utf-8")) # Return - Comença la música
          time.sleep(0.3)
          s.send(bytes("<command>keymatrixup 7 128</command>","utf-8"))
          time.sleep(2)
          s.send(bytes("<command>set fastforward on</command>","utf-8"))

          # Alguns fitxers tarden més en carregar
          intents = 0
          parar = False
          while intents <3 and not parar:
            # Comencem a gravar
            print("!!! Comencem a gravar")
            s.send(bytes("<command>record start -audioonly /home/jepsuse/borram/bor.wav</command>","utf-8"))
            time.sleep(4)
            # Capturem la pantalla per provar si s'ha penjat
            s.send(bytes("<command>screenshot -raw /home/jepsuse/borram/bor_abans.png</command>","utf-8"))
            s.send(bytes("<command>record stop</command>","utf-8"))
            # Sortim de la cançó si encara està sonant, prement esc
            s.send(bytes("<command>set fastforward off</command>","utf-8"))
            s.send(bytes("<command>keymatrixdown 7 4</command>","utf-8")) # Esc - Sortim de la reproducció
            time.sleep(0.3)
            s.send(bytes("<command>keymatrixup 7 4</command>","utf-8"))
            time.sleep(0.5)
            # Esperem que torni a MSXDOS
            time.sleep(4)
            s.send(bytes("<command>screenshot -raw /home/jepsuse/borram/bor_despres.png</command>","utf-8"))

            # Inspeccionem la potencia del so gravat
            try:
              wf = wave.open('/home/jepsuse/borram/bor.wav','rb')
              num_frames = wf.getnframes()

              # Read the frames into a byte array
              audio_data = wf.readframes(num_frames)
                
              # Convert the byte array to a numpy array
              audio_array = np.frombuffer(audio_data, dtype=np.int16)

              # Calculate the average volume
              average_volume = np.mean(np.abs(audio_array))
              max_volume = np.max(np.abs(audio_array))
              print(f"!!! volum: {average_volume} - {max_volume}")
              wf.close()

              if average_volume < 600:
                # Ho tornem a intentar
                intents += 1
                print("!!! Sembla que no ha sonat. Ho reintentem")
                # Apretem el cursor cap amunt i l'enter
                s.send(bytes("<command>keymatrixdown 8 32</command>","utf-8")) # Esc - Sortim de la reproducció
                time.sleep(0.3)
                s.send(bytes("<command>keymatrixup 8 32</command>","utf-8"))
                time.sleep(0.5)
                s.send(bytes("<command>keymatrixdown 7 128</command>","utf-8")) # Return - Comença la música
                time.sleep(0.3)
                s.send(bytes("<command>keymatrixup 7 128</command>","utf-8"))
                time.sleep(2)
                s.send(bytes("<command>set fastforward on</command>","utf-8"))
              else:
                parar = True

              # Hem d'esborrar el fitxer perquè si es penja i no s'ha borrat el va detectant com a bo.
              try:
                os.remove('/home/jepsuse/borram/bor.wav')
              except:
                print(f"$$$ No hem pogut eliminar el fitxer de so bor.wav")

              # Mirem que les imatges capturades siguin diferents, voldrà dir que no s'ha penjat
              dif_imatges = calcula_dif_imatges('/home/jepsuse/borram/bor_abans.png', '/home/jepsuse/borram/bor_despres.png')
            except FileNotFoundError:
              # Es deu haver penjat el openMSX. Faig un reset de l'aplicació
              print("!!! Anem a fer un reset de l'openMSX " +  "\U0001F92F")
              s.close()
              func_kill_openMSX()
              pid, func_kill_openMSX = start_and_kill_process(['/home/jepsuse/MSX/openMSX/derived/openmsx','-machine','Panasonic_FS-A1ST','-ext','moonsound', '-diska', '/home/jepsuse/MSX/roboplay/dsk/'])
              s.connect('/tmp/openmsx-jepsuse/socket.' + pid_openMSX)
              dif_imatges = 0
              average_volume = 0

            s.send(bytes("<command>set fastforward off</command>","utf-8"))

            numPorto = numPorto + 1
            s.close()

            # Processem el resultat
            if dif_imatges < 10:
              fitxer_ko.write(cami_msx + nomfitx + " ;-; MSX penjat \n")
              print("!!! S'ha penjat el MSX")
            elif average_volume>500 and max_volume>7000:
              fitxer_ok.write(cami_msx + nomfitx + "\n")
              print("=== fitxer ok " + "\U0001F600\U0001F600\U0001F600")
            else:
              fitxer_ko.write(cami_msx + nomfitx + "\n")
              print("=== fitxer ko " + "\U0001F480\U0001F480\U0001F480")

            # Elinimen fitxer creat per no saturar el disquet
            for fitxmbm in fitxmbms:
              [pathmbm, nommbm] = os.path.split(fitxmbm)
              try:
                os.remove('/home/jepsuse/MSX/roboplay/dsk/WozBlast/OPL4/TotMusic/' + cami_msx + '/' + nommbm)
              except:
                print(f"$$$ No hem pogut eliminar el fitxer {fitxmbm}")

            # Eliminem les imatges
            try:
              os.remove('/home/jepsuse/borram/bor_abans.png')
              os.remove('/home/jepsuse/borram/bor_despres.png')
            except:
              print(f"$$$ No hem pogut eliminar el fitxer de les imatges")

      else:
        print(f"$$$ fitxer {fitxer} no és de so")
        numPorto += 1
    else:
      numPorto += 1
      print(f"$$$ fitxer {fitxer} és massa gran {os.path.getsize(fitxer.strip())}")
      fitxer_ko.write(fitxer + " - massa gran \n")
    # Calculem els temps i els imprimim
    loop_end = time.time()
    print(f"=== iteració {numPorto} ha tardat {loop_end-loop_start}. El promig és de {(loop_end-start_time)/(numPorto)} i estimem que queda {(1000-numPorto)*(loop_end-start_time)/(numPorto)} segons")
  else:
    print(f"!!! Fitxer {fitxer} no trobat")
    fitxer_ko.write(fitxer + " - No Trobat\n")

fitxer_ok.close()
fitxer_ko.close()
func_kill_openMSX()
