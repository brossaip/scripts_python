# Programa que cridarà al configure_OPL3 i crearà diferents waves amb els diferents valors dels paràmetres 
# els paràmetres s'han agafat de l'ajusta_genetic_FM.
# Hauré de crear un altre programa que crearà els inicials, els que són fixats.

import subprocess
import random
import os
import sys


poblacio_inicial_instruments_furnace = [
[485, 3, 0, 7,1,0,63,0,15,0,0,15,0,15,15,0,1,1,27,0,15,1,0,0,0,15,15,0,0,2,20,0,15,0,0,0,0,15,4,3,0,1,2,0,15,0,0,0,0,15,9,6,], #instruments/OPL/Psuedo-PoKEY Perodic Noise (4OP).fui
[485, 3, 0, 4,1,0,26,0,12,0,0,11,0,12,11,0,1,5,0,1,7,0,0,3,0,11,10,6,0,5,18,0,11,1,0,7,0,15,12,0,0,1,2,0,9,0,0,4,0,7,10,0,], #instruments/OPL/OPL3-TwinkleKey.fui
[485, 3, 2, 5,1,1,23,0,15,0,0,5,0,12,9,0,1,0,21,0,13,0,0,3,0,12,9,0,0,9,38,0,9,0,0,6,0,13,9,0,0,0,0,0,13,0,0,3,0,15,10,0,], #instruments/OPL/OPL3_Springybass.fui
[485, 3, 0, 7,1,10,40,0,15,0,0,7,0,2,6,0,1,0,26,0,15,0,0,5,0,6,5,0,0,0,32,0,14,0,0,6,0,3,4,0,0,1,0,0,15,0,0,5,0,1,9,0,], #instruments/OPL/OPL3_Slap_Bass.fui
[485, 3, 0, 0,1,0,0,0,15,1,0,0,0,0,15,0,1,0,0,0,15,0,0,0,0,0,15,0,0,0,0,0,15,0,0,0,0,15,15,0,0,0,0,0,11,0,0,5,0,2,7,6,], #instruments/OPL/OPL3-PeriodicNoise.fui
[485, 3, 2, 0,1,1,0,0,9,0,0,5,0,2,15,2,1,1,35,0,11,0,0,8,0,3,15,0,0,5,14,0,12,1,0,5,0,4,15,0,0,1,0,0,7,0,0,2,0,0,15,0,], #instruments/OPL/OPL3-FakeFalcomString.fui
[485, 3, 0, 6,1,3,24,0,8,1,1,3,0,2,7,0,1,1,25,0,9,0,0,5,0,1,6,0,0,1,24,1,7,0,0,2,0,1,7,0,0,1,0,0,11,0,0,0,0,15,8,0,], #instruments/OPL/OPL3-FakeFalcomGuitar.fui
[485, 3, 0, 0,1,0,0,0,15,1,0,0,0,0,0,0,1,0,0,0,15,0,0,0,0,0,0,0,0,0,0,0,15,0,0,0,0,15,0,0,0,0,0,0,11,0,0,6,0,3,7,6,], #instruments/OPL/OPL3-clap.fui
[485, 3, 2, 0,1,0,0,0,15,0,0,5,0,3,8,0,1,0,26,0,15,0,0,6,0,2,7,0,0,4,26,1,8,1,0,7,0,9,9,0,0,1,0,0,15,0,0,2,0,15,11,0,], #instruments/OPL/OPL3_BigBass.fui
[485, 3, 0, 5,1,6,28,0,15,0,0,4,0,4,6,0,1,0,20,0,15,0,0,4,0,3,6,0,0,5,45,0,15,0,0,6,0,4,6,0,0,1,6,0,15,0,0,2,0,15,8,0,], #instruments/OPL/opl3_4op_bass_1.fui
[485, 3, 4, 3,1,1,27,0,15,0,2,1,0,15,5,0,1,10,41,0,15,0,0,2,0,6,5,0,0,1,2,0,15,0,0,3,0,15,7,0,0,1,2,0,15,0,0,3,0,15,7,0,], #instruments/OPL/DX7 Electric Piano.fui
[485, 3, 6, 0,1,5,13,1,9,0,1,6,0,7,5,4,1,2,19,0,7,0,0,3,0,2,4,0,0,4,41,1,15,0,0,0,0,15,4,2,0,2,2,1,15,0,0,3,0,15,5,0,], #instruments/OPL/Crystal.fui
[485, 3, 4, 4,1,0,39,1,5,0,0,0,0,0,0,2,1,5,52,0,5,0,0,0,0,4,5,4,0,1,9,0,5,0,0,4,0,5,4,1,0,1,2,0,5,0,0,0,0,5,4,0,], #instruments/OPL/Chorus Organ.fui
[485, 3, 0, 0,1,12,21,1,15,0,2,6,0,3,0,0,1,0,20,0,15,0,0,9,0,2,0,0,0,5,40,1,15,0,0,8,0,2,6,0,0,2,3,0,15,0,0,2,0,15,6,0,], #instruments/OPL/4op Bass.fui
[485, 3, 6, 0,1,5,0,0,15,0,0,4,0,15,7,6,1,4,0,0,15,0,0,3,0,12,5,6,0,7,4,0,15,0,0,2,0,14,3,6,0,6,0,0,15,0,0,4,0,15,7,6,], #instruments/OPL/OPL3-MajorSquare.fui
[485, 3, 0, 7,1,2,4,0,15,0,0,7,0,3,6,1,1,1,12,0,15,1,0,6,0,2,6,1,0,0,12,1,15,0,0,3,0,3,6,0,0,1,10,0,15,0,0,1,0,15,6,0,], #instruments/OPL/opl3_4op_rhythm_guitar_2.fui
[485, 3, 0, 7,1,2,4,0,15,0,0,7,0,3,6,1,1,0,12,0,15,1,0,6,0,2,6,3,0,0,12,1,15,0,0,3,0,3,6,0,0,1,10,0,15,0,0,1,0,15,6,0,], #instruments/OPL/opl3_4op_rhythm_guitar_1.fui
[485, 3, 4, 6,0,3,25,0,13,0,1,1,0,15,5,0,0,14,32,0,15,0,1,2,0,14,5,0,0,1,4,1,15,0,2,1,0,15,5,0,0,4,12,0,15,1,0,3,0,15,5,0,], #instruments/OPL/opl3_4op_piano_bell.fui
[485, 3, 0, 6,1,2,5,0,15,0,0,8,0,10,12,0,1,0,6,0,15,1,0,3,0,3,7,2,0,2,35,1,15,0,0,3,0,6,10,1,0,1,10,1,15,0,0,0,0,0,6,0,], #instruments/OPL/opl3_4op_lead_guitar.fui
[485, 3, 0, 7,1,5,34,0,15,0,0,2,0,9,15,1,1,0,18,0,15,1,0,0,0,0,15,0,0,2,28,1,4,0,0,3,0,15,15,2,0,0,10,1,7,0,0,0,0,15,6,0,], #instruments/OPL/opl3_4op_growl_synth.fui
[485, 3, 4, 7,1,8,0,0,15,0,0,5,0,10,0,0,1,2,15,0,15,0,0,9,0,15,6,7,0,2,6,0,15,0,0,6,0,10,10,0,0,1,0,0,15,0,0,0,0,0,0,0,], #instruments/OPL/opl3_4op_drum_tom.fui
[485, 3, 0, 7,1,4,0,0,15,0,0,3,0,4,0,0,1,2,0,0,15,0,0,10,0,9,6,0,0,2,8,0,15,0,0,6,0,7,10,0,0,1,0,0,15,0,0,0,0,0,0,0,], #instruments/OPL/opl3_4op_drum_snare.fui
[485, 3, 0, 3,1,3,0,0,15,0,1,3,0,2,7,2,1,1,0,0,15,0,0,8,0,1,15,2,0,2,0,0,15,0,0,4,0,8,4,6,0,0,4,0,15,0,0,5,0,15,9,6,], #instruments/OPL/opl3_4op_drum_power_snare.fui
[485, 3, 0, 7,1,4,0,0,15,0,0,5,0,8,0,0,1,2,0,0,15,0,0,11,0,13,6,0,0,2,8,0,15,0,0,8,0,11,10,0,0,1,0,0,15,0,0,0,0,0,0,0,], #instruments/OPL/opl3_4op_drum_kick.fui
[485, 3, 0, 0,1,5,32,0,2,1,0,3,0,11,4,0,1,3,18,0,9,1,0,1,0,15,5,0,0,0,23,0,15,1,0,4,0,5,4,0,0,1,0,0,15,0,0,5,0,1,5,0,], #instruments/OPL/OPL3-4opsitar.fui
[485, 3, 0, 0,1,9,25,0,5,0,0,2,0,5,10,0,1,1,26,0,15,0,0,2,0,6,7,0,0,6,41,0,12,0,0,1,0,7,7,0,0,1,0,0,6,0,0,2,0,1,13,0,], #instruments/OPL/OPL3-4opharmonica.fui
[485, 3, 0, 0,1,1,12,0,8,0,1,7,0,4,15,0,1,1,12,1,6,1,0,5,0,1,15,1,0,7,48,0,7,0,0,5,0,2,15,0,0,1,0,0,8,0,0,15,0,0,15,0,], #instruments/OPL/OPL3-4opbrass11.fui
[485, 3, 4, 4,0,2,22,1,15,0,0,0,0,0,0,0,0,2,22,0,15,1,2,0,0,0,0,0,0,2,10,1,7,0,0,6,0,1,6,0,0,2,10,0,7,1,1,6,0,1,4,0,], #instruments/OPL/opl3_4op_strings.fui
[485, 3, 4, 6,0,3,25,0,13,0,1,1,0,15,5,0,0,14,32,0,15,0,1,2,0,14,5,0,0,1,4,1,15,0,2,1,0,15,5,0,0,4,12,0,15,1,0,3,0,15,5,0,], #instruments/OPL/pianoBell.fui    
####
[700, 3, 0, 7,1,0,63,0,15,0,0,15,0,15,15,0,1,1,27,0,15,1,0,0,0,15,15,0,0,2,20,0,15,0,0,0,0,15,4,3,0,1,2,0,15,0,0,0,0,15,9,6,], #instruments/OPL/Psuedo-PoKEY Perodic Noise (4OP).fui
[700, 3, 0, 4,1,0,26,0,12,0,0,11,0,12,11,0,1,5,0,1,7,0,0,3,0,11,10,6,0,5,18,0,11,1,0,7,0,15,12,0,0,1,2,0,9,0,0,4,0,7,10,0,], #instruments/OPL/OPL3-TwinkleKey.fui
[700, 3, 2, 5,1,1,23,0,15,0,0,5,0,12,9,0,1,0,21,0,13,0,0,3,0,12,9,0,0,9,38,0,9,0,0,6,0,13,9,0,0,0,0,0,13,0,0,3,0,15,10,0,], #instruments/OPL/OPL3_Springybass.fui
[700, 3, 0, 7,1,10,40,0,15,0,0,7,0,2,6,0,1,0,26,0,15,0,0,5,0,6,5,0,0,0,32,0,14,0,0,6,0,3,4,0,0,1,0,0,15,0,0,5,0,1,9,0,], #instruments/OPL/OPL3_Slap_Bass.fui
[700, 3, 0, 0,1,0,0,0,15,1,0,0,0,0,15,0,1,0,0,0,15,0,0,0,0,0,15,0,0,0,0,0,15,0,0,0,0,15,15,0,0,0,0,0,11,0,0,5,0,2,7,6,], #instruments/OPL/OPL3-PeriodicNoise.fui
[700, 3, 2, 0,1,1,0,0,9,0,0,5,0,2,15,2,1,1,35,0,11,0,0,8,0,3,15,0,0,5,14,0,12,1,0,5,0,4,15,0,0,1,0,0,7,0,0,2,0,0,15,0,], #instruments/OPL/OPL3-FakeFalcomString.fui
[700, 3, 0, 6,1,3,24,0,8,1,1,3,0,2,7,0,1,1,25,0,9,0,0,5,0,1,6,0,0,1,24,1,7,0,0,2,0,1,7,0,0,1,0,0,11,0,0,0,0,15,8,0,], #instruments/OPL/OPL3-FakeFalcomGuitar.fui
[700, 3, 0, 0,1,0,0,0,15,1,0,0,0,0,0,0,1,0,0,0,15,0,0,0,0,0,0,0,0,0,0,0,15,0,0,0,0,15,0,0,0,0,0,0,11,0,0,6,0,3,7,6,], #instruments/OPL/OPL3-clap.fui
[700, 3, 2, 0,1,0,0,0,15,0,0,5,0,3,8,0,1,0,26,0,15,0,0,6,0,2,7,0,0,4,26,1,8,1,0,7,0,9,9,0,0,1,0,0,15,0,0,2,0,15,11,0,], #instruments/OPL/OPL3_BigBass.fui
[700, 3, 0, 5,1,6,28,0,15,0,0,4,0,4,6,0,1,0,20,0,15,0,0,4,0,3,6,0,0,5,45,0,15,0,0,6,0,4,6,0,0,1,6,0,15,0,0,2,0,15,8,0,], #instruments/OPL/opl3_4op_bass_1.fui
[700, 3, 4, 3,1,1,27,0,15,0,2,1,0,15,5,0,1,10,41,0,15,0,0,2,0,6,5,0,0,1,2,0,15,0,0,3,0,15,7,0,0,1,2,0,15,0,0,3,0,15,7,0,], #instruments/OPL/DX7 Electric Piano.fui
[700, 3, 6, 0,1,5,13,1,9,0,1,6,0,7,5,4,1,2,19,0,7,0,0,3,0,2,4,0,0,4,41,1,15,0,0,0,0,15,4,2,0,2,2,1,15,0,0,3,0,15,5,0,], #instruments/OPL/Crystal.fui
[700, 3, 4, 4,1,0,39,1,5,0,0,0,0,0,0,2,1,5,52,0,5,0,0,0,0,4,5,4,0,1,9,0,5,0,0,4,0,5,4,1,0,1,2,0,5,0,0,0,0,5,4,0,], #instruments/OPL/Chorus Organ.fui
[700, 3, 0, 0,1,12,21,1,15,0,2,6,0,3,0,0,1,0,20,0,15,0,0,9,0,2,0,0,0,5,40,1,15,0,0,8,0,2,6,0,0,2,3,0,15,0,0,2,0,15,6,0,], #instruments/OPL/4op Bass.fui
[700, 3, 6, 0,1,5,0,0,15,0,0,4,0,15,7,6,1,4,0,0,15,0,0,3,0,12,5,6,0,7,4,0,15,0,0,2,0,14,3,6,0,6,0,0,15,0,0,4,0,15,7,6,], #instruments/OPL/OPL3-MajorSquare.fui
[700, 3, 0, 7,1,2,4,0,15,0,0,7,0,3,6,1,1,1,12,0,15,1,0,6,0,2,6,1,0,0,12,1,15,0,0,3,0,3,6,0,0,1,10,0,15,0,0,1,0,15,6,0,], #instruments/OPL/opl3_4op_rhythm_guitar_2.fui
[700, 3, 0, 7,1,2,4,0,15,0,0,7,0,3,6,1,1,0,12,0,15,1,0,6,0,2,6,3,0,0,12,1,15,0,0,3,0,3,6,0,0,1,10,0,15,0,0,1,0,15,6,0,], #instruments/OPL/opl3_4op_rhythm_guitar_1.fui
[700, 3, 4, 6,0,3,25,0,13,0,1,1,0,15,5,0,0,14,32,0,15,0,1,2,0,14,5,0,0,1,4,1,15,0,2,1,0,15,5,0,0,4,12,0,15,1,0,3,0,15,5,0,], #instruments/OPL/opl3_4op_piano_bell.fui
[700, 3, 0, 6,1,2,5,0,15,0,0,8,0,10,12,0,1,0,6,0,15,1,0,3,0,3,7,2,0,2,35,1,15,0,0,3,0,6,10,1,0,1,10,1,15,0,0,0,0,0,6,0,], #instruments/OPL/opl3_4op_lead_guitar.fui
[700, 3, 0, 7,1,5,34,0,15,0,0,2,0,9,15,1,1,0,18,0,15,1,0,0,0,0,15,0,0,2,28,1,4,0,0,3,0,15,15,2,0,0,10,1,7,0,0,0,0,15,6,0,], #instruments/OPL/opl3_4op_growl_synth.fui
[700, 3, 4, 7,1,8,0,0,15,0,0,5,0,10,0,0,1,2,15,0,15,0,0,9,0,15,6,7,0,2,6,0,15,0,0,6,0,10,10,0,0,1,0,0,15,0,0,0,0,0,0,0,], #instruments/OPL/opl3_4op_drum_tom.fui
[700, 3, 0, 7,1,4,0,0,15,0,0,3,0,4,0,0,1,2,0,0,15,0,0,10,0,9,6,0,0,2,8,0,15,0,0,6,0,7,10,0,0,1,0,0,15,0,0,0,0,0,0,0,], #instruments/OPL/opl3_4op_drum_snare.fui
[700, 3, 0, 3,1,3,0,0,15,0,1,3,0,2,7,2,1,1,0,0,15,0,0,8,0,1,15,2,0,2,0,0,15,0,0,4,0,8,4,6,0,0,4,0,15,0,0,5,0,15,9,6,], #instruments/OPL/opl3_4op_drum_power_snare.fui
[700, 3, 0, 7,1,4,0,0,15,0,0,5,0,8,0,0,1,2,0,0,15,0,0,11,0,13,6,0,0,2,8,0,15,0,0,8,0,11,10,0,0,1,0,0,15,0,0,0,0,0,0,0,], #instruments/OPL/opl3_4op_drum_kick.fui
[700, 3, 0, 0,1,5,32,0,2,1,0,3,0,11,4,0,1,3,18,0,9,1,0,1,0,15,5,0,0,0,23,0,15,1,0,4,0,5,4,0,0,1,0,0,15,0,0,5,0,1,5,0,], #instruments/OPL/OPL3-4opsitar.fui
[700, 3, 0, 0,1,9,25,0,5,0,0,2,0,5,10,0,1,1,26,0,15,0,0,2,0,6,7,0,0,6,41,0,12,0,0,1,0,7,7,0,0,1,0,0,6,0,0,2,0,1,13,0,], #instruments/OPL/OPL3-4opharmonica.fui
[700, 3, 0, 0,1,1,12,0,8,0,1,7,0,4,15,0,1,1,12,1,6,1,0,5,0,1,15,1,0,7,48,0,7,0,0,5,0,2,15,0,0,1,0,0,8,0,0,15,0,0,15,0,], #instruments/OPL/OPL3-4opbrass11.fui
[700, 3, 4, 4,0,2,22,1,15,0,0,0,0,0,0,0,0,2,22,0,15,1,2,0,0,0,0,0,0,2,10,1,7,0,0,6,0,1,6,0,0,2,10,0,7,1,1,6,0,1,4,0,], #instruments/OPL/opl3_4op_strings.fui
[700, 3, 4, 6,0,3,25,0,13,0,1,1,0,15,5,0,0,14,32,0,15,0,1,2,0,14,5,0,0,1,4,1,15,0,2,1,0,15,5,0,0,4,12,0,15,1,0,3,0,15,5,0,], #instruments/OPL/pianoBell.fui
]

for instrument in poblacio_inicial_instruments_furnace:
    comanda = ['./configureOPL3_generateFM']
    instrument_str = [str(f) for f in instrument]
    comanda = comanda + instrument_str
    
    subprocess.run(comanda)

# He de provar que dels generats amb totes les combinacions crear una taula amb la meva mesura de discrepància i avaluar si és un bon criteri o no
# També he de mirar si en els efectes wav que vull provar estan a la mateixa amplitud o de normalitzar el senyal per poder comparar amb els del món real
# Ho hauré de normalitzar per poder comparar
# import numpy as np
# from scipy.io import wavfile

# def normalize_wav(input_file, output_file=None):
#     # Read the WAV file
#     sample_rate, data = wavfile.read(input_file)

#     # Handle stereo by converting to mono (optional, if desired)
#     if len(data.shape) > 1:
#         data = data.mean(axis=1)  # Convert to mono by averaging channels

#     # Normalize the data to be between -1 and 1
#     max_value = np.max(np.abs(data))  # Find the maximum absolute value
#     normalized_data = data / max_value  # Scale the data

#     # Optionally write the normalized data to a new WAV file
#     if output_file:
#         # Scale back to original data type range and write
#         scaled_data = (normalized_data * np.iinfo(data.dtype).max).astype(data.dtype)
#         wavfile.write(output_file, sample_rate, scaled_data)

#     return sample_rate, normalized_data

# # Example usage
# input_wav = "input.wav"
# output_wav = "output_normalized.wav"

# # Normalize the WAV and save it
# sample_rate, normalized_audio = normalize_wav(input_wav, output_wav)
# print(f"Normalized audio written to {output_wav}")

# Crec que adaptaré el genetic_algorithm perquè treballi amb el Nuked i em funcions normalitzades abans d'haver d'entrenar la xarxa neuronal
# Però sí que he de trobar la mesura correcta amb aquests exemples que tinc i el wav (https://www.wavsource.com/sfx/sfx.htm)
