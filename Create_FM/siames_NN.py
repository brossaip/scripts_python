# Per entrenar no he de generar mostres a l'atzar, que sí, però ha d'estar ben compensat el número de mostres que s'assemblen que les que no s'assemblen.
import os
import librosa
import torch
import torch.nn as nn
import torchaudio
import torchvision.models as models
from torchvision.models import ResNet18_Weights
import crea_wav_randomParams
import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import euclidean_distances

# Device configuration
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Define the directory containing audio files
audio_dir = '/home/jbd/Documents/JBD/Nuked-OPL3/petita_seleccio/'

# Function to extract numerical values from the filename
def extract_numeric_values(filename):
    # Remove 'norm_to_' prefix and split by underscores
    values = filename.replace('norm_to_', '').split('_')
    values[-1] = values[-1][:-4]
    # print(f"*** values: {values}")
    return np.array([int(v) for v in values])

params_max = [param[1]-1 for param in crea_wav_randomParams.parametres]

# Function to calculate distance based on filename
def calculate_filename_distance(file1, file2):
    values1 = extract_numeric_values(file1)
    values1 = values1 / params_max
    values2 = extract_numeric_values(file2)
    values2 = values2 / params_max
    # He de normalitzar aquests valors
    # Compute Euclidean distance between numerical sequences
    return np.linalg.norm(values1 - values2)

# Load and process WAV files
def load_audio_files(directory):
    audio_files = []
    filenames = []

    for file in os.listdir(directory):
        if file.endswith('.wav'):
            filepath = os.path.join(directory, file)
            waveform, sr = torchaudio.load(filepath)
            audio_files.append(waveform)
            filenames.append(file)

    return audio_files, filenames

# Convert audio to Mel spectrogram
def audio_to_mel(audio_waveform, sr=44100, n_fft=1024, hop_length=512, n_mels=64):
    mel_spec_transform = torchaudio.transforms.MelSpectrogram(
        sample_rate=sr, n_fft=n_fft, hop_length=hop_length, n_mels=n_mels
    )
    mel_spec = mel_spec_transform(audio_waveform)
    mel_spec = torch.log(mel_spec + 1e-9)  # Avoid log(0)
    return mel_spec


def compute_librosa_features(filepath, sr=44100):
    y, sr = librosa.load(filepath, sr=sr)

    # Example features
    spectral_centroid = librosa.feature.spectral_centroid(y=y, sr=sr).mean()
    zero_crossing_rate = librosa.feature.zero_crossing_rate(y).mean()
    rmse = librosa.feature.rms(y=y).mean()
    spectral_bandwidth = librosa.feature.spectral_bandwidth(y=y, sr=sr).mean()

    return np.array([spectral_centroid, zero_crossing_rate, rmse, spectral_bandwidth], dtype=np.float32)


# Define the Siamese network using ResNet for feature extraction
class SiameseNetwork(nn.Module):
    def __init__(self, scalar_feature_dim=4):
        super(SiameseNetwork, self).__init__()
        
        # Convolutional feature extractor
        self.conv_layers = nn.Sequential(
            nn.Conv2d(1, 32, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),

            nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),

            nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(128),
            nn.ReLU(),

            nn.AdaptiveAvgPool2d((8, 8))  # Always outputs (8,8) feature maps
        )

        # Fully connected layers for feature embedding
        self.fc_audio = nn.Linear(128*8*8, 256)

        # Fully connected layer for scalar features
        self.fc_scalar = nn.Linear(scalar_feature_dim, 64)

        # Final embedding layer
        self.fc_final = nn.Linear(256 + 64, 256)

    def forward_once(self, x, scalar_features):
        x = self.conv_layers(x)  # Pass through convolutional layers
        x = torch.flatten(x, 1)  # Flatten for FC layer
        x = self.fc_audio(x)  # Apply fully connected layer
        # Process scalar features
        scalar_out = self.fc_scalar(scalar_features)

        # Concatenate both embeddings
        combined = torch.cat((x, scalar_out), dim=1)
        output = self.fc_final(combined)

        return output

    def forward(self, input1, input2, scalar1, scalar2):
        output1 = self.forward_once(input1, scalar1)
        output2 = self.forward_once(input2, scalar2)
        return output1, output2

# Load audio data and filenames
audio_data, audio_filenames = load_audio_files(audio_dir)

# Convert audio to mel spectrograms
mel_specs = [audio_to_mel(audio) for audio in audio_data]

# Initialize the model
model = SiameseNetwork()

# Check if a saved model exists and load it
model_path = "siamese_model.pth"
if os.path.exists(model_path):
    print(f"**** Loading model from {model_path}...")
    model.load_state_dict(torch.load(model_path, map_location=device))
else:
    print("No saved model found. Starting training from scratch.")

model.eval()

# Generate embeddings for all audio file
def compute_embeddings(mel_specs, filenames):
    embeddings = []
    scalar_features_list = []

    with torch.no_grad():
        for spec, filename in zip(mel_specs, filenames):
            if len(spec.shape) == 2:
                spec = spec.unsqueeze(0)  # Add channel dimension: [1, H, W]
            
            spec = spec.unsqueeze(0).to(device)  # Add batch dimension: [1, 1, H, W]
            
            # Load scalar features
            scalar_features = compute_librosa_features(os.path.join(audio_dir, filename))
            scalar_features = torch.tensor(scalar_features, dtype=torch.float32).unsqueeze(0).to(device)

            embedding = model.forward_once(spec, scalar_features)
            embeddings.append(embedding.cpu().numpy())
            scalar_features_list.append(scalar_features.cpu().numpy())

    return np.vstack(embeddings), np.vstack(scalar_features_list)

audio_embeddings, scalar_features = compute_embeddings(mel_specs, audio_filenames)

# Calculate the distance matrix
def compute_distance_matrix(audio_embeddings, audio_filenames):
    N = len(audio_filenames)
    distance_matrix = np.zeros((N, N))

    for i in range(N):
        for j in range(i + 1, N):
            # Calculate filename-based distance
            # print(f"*** {audio_filenames[i]} - {audio_filenames[j]} ")
            name_distance = calculate_filename_distance(audio_filenames[i], audio_filenames[j])

            # Calculate embedding-based distance (Euclidean)
            embed_distance = np.linalg.norm(audio_embeddings[i] - audio_embeddings[j])

            # Combine distances (weighted sum)
            total_distance = 0.5 * name_distance + 0.5 * embed_distance

            distance_matrix[i, j] = distance_matrix[j, i] = total_distance

    return distance_matrix

# Compute and save the distance matrix
distance_matrix = compute_distance_matrix(audio_embeddings, audio_filenames)

np.savetxt('audio_distance_matrix.txt', distance_matrix, fmt='%.6f', delimiter=';')

# # Display distance matrix
# import matplotlib.pyplot as plt
# import seaborn as sns

# plt.figure(figsize=(12, 10))
# sns.heatmap(distance_matrix, annot=False, cmap="viridis")
# plt.title("Audio Distance Matrix")
# plt.show()

# Contrastive Loss Function
class ContrastiveLoss(nn.Module):
    def __init__(self, margin=1.0):
        super(ContrastiveLoss, self).__init__()
        self.margin = margin

    def forward(self, output1, output2, label):
        # Compute Euclidean distance between output pairs
        euclidean_distance = torch.nn.functional.pairwise_distance(output1, output2)
        
        # Contrastive loss function formula
        loss = torch.mean(
            (1 - label) * torch.pow(euclidean_distance, 2) +  # For similar pairs
            label * torch.pow(torch.clamp(self.margin - euclidean_distance, min=0.0), 2)  # For dissimilar pairs
        )
        return loss

# Prepare dataset (pairs of audio embeddings)
def create_pairs(audio_embeddings, scalar_features, labels):
    pairs = []
    scalar_pairs = []
    targets = []

    for i in range(len(labels)):
        for j in range(i + 1, len(labels)):
            pairs.append((audio_embeddings[i], audio_embeddings[j]))
            scalar_pairs.append((scalar_features[i], scalar_features[j]))

            # Define labels (1 if similar, 0 if dissimilar)
            if calculate_filename_distance(audio_filenames[i], audio_filenames[j]) < 1:
                targets.append(1)  # Similar pairs
            else:
                targets.append(0)  # Dissimilar pairs

    return torch.tensor(np.array(pairs)), torch.tensor(np.array(scalar_pairs)), torch.tensor(np.array(targets))

# Create pairs and labels
audio_embeddings_tensor = torch.tensor(audio_embeddings, dtype=torch.float32)
pairs, scalar_features, targets = create_pairs(audio_embeddings_tensor, scalar_features, audio_filenames)

# Training loop setup
model = SiameseNetwork().to(device)
criterion = ContrastiveLoss(margin=3)
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

# Training the model
def train(model, pairs, targets, scalar_features, epochs=10, save_every=2000):
    model.train()
    
    step = 0
    for epoch in range(epochs):
        total_loss = 0.0
        for idx, ((input1, input2), label, (scalar1, scalar2)) in enumerate(zip(pairs, targets, scalar_features)):
            # Ensure input shape [batch, channels, height, width]
            input1 = input1.view(1, 1, 64, -1).to(device)  # Reshape input1 correctly
            input2 = input2.view(1, 1, 64, -1).to(device)  # Reshape input2 correctly
            scalar1 = scalar1.clone().detach().to(device).unsqueeze(0)
            scalar2 = scalar2.clone().detach().to(device).unsqueeze(0)

            
            label = label.to(device)

            optimizer.zero_grad()

            # Forward pass
            output1, output2 = model(input1, input2, scalar1, scalar2)

            # Compute loss
            loss = criterion(output1, output2, label.float())
            loss.backward()

            optimizer.step()
            total_loss += loss.item()
            step += 1

            if step % save_every == 0:
                print(f"Step {step}: Saving model to {model_path}...")
                torch.save(model.state_dict(), model_path)

            if idx % 10 == 0:
                print(f"Epoch [{epoch+1}/{epochs}], Step [{idx}/{len(pairs)}], Loss: {loss.item():.4f}")

        print(f"Epoch [{epoch+1}/{epochs}] Total Loss: {total_loss:.4f}")

# Start training
train(model, pairs, targets, scalar_features, epochs=40)

torch.save(model.state_dict(), "siamese_model.pth")

def compute_similarity(model, file1, file2):
    model.eval()

    # Load audio files and convert to mel spectrograms
    waveform1, _ = torchaudio.load(os.path.join(audio_dir, file1))
    waveform2, _ = torchaudio.load(os.path.join(audio_dir, file2))

    # Convert to mel spectrogram
    input1 = audio_to_mel(waveform1)
    input2 = audio_to_mel(waveform2)

    input1 = input1.unsqueeze(0).to(device)
    input2 = input2.unsqueeze(0).to(device)

    if input1.dim() == 3:
        input1 = input1.unsqueeze(0)
        input2 = input2.unsqueeze(0)

    # Compute scalar features
    scalar1 = compute_librosa_features(os.path.join(audio_dir, file1))
    scalar2 = compute_librosa_features(os.path.join(audio_dir, file2))

    scalar1 = torch.tensor(scalar1, dtype=torch.float32).unsqueeze(0).to(device)
    scalar2 = torch.tensor(scalar2, dtype=torch.float32).unsqueeze(0).to(device)

    with torch.no_grad():
        output1, output2 = model(input1, input2, scalar1, scalar2)

        # Compute Euclidean distance
        distance = torch.nn.functional.pairwise_distance(output1, output2)
    
    return distance.item()

# Example usage
N = len(audio_filenames)
distance_matrix = np.zeros((N, N))

results_matrix = pd.DataFrame(index=audio_filenames, columns=audio_filenames)
results_matrix_orig = pd.DataFrame(index=audio_filenames, columns=audio_filenames)
for i in range(N):
    for j in range(i + 1, N):
        dist = compute_similarity(model, audio_filenames[i], audio_filenames[j])
        distance_matrix[i, j] = distance_matrix[j, i] = dist
        results_matrix.loc[audio_filenames[i],audio_filenames[j]] = dist
        results_matrix_orig.loc[audio_filenames[i],audio_filenames[j]] = calculate_filename_distance(audio_filenames[i],audio_filenames[j])

np.savetxt('siamese_distance_matrix.txt', distance_matrix, fmt='%.6f', delimiter=';')
results_matrix.to_csv("res_siames.csv")
results_matrix_orig.to_csv("res_orig.csv")

import matplotlib.pyplot as plt
import seaborn as sns

plt.figure(figsize=(12, 10))
sns.heatmap(distance_matrix, annot=False, cmap="viridis")
plt.title("Audio Similarity Matrix")
plt.show()
