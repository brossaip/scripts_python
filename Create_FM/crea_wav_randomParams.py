# Programa que cridarà al configure_OPL3 i crearà diferents waves amb els diferents valors dels paràmetres 
# els paràmetres s'han agafat de l'ajusta_genetic_FM.
# Hauré de crear un altre programa que crearà els inicials, els que són fixats.

import subprocess
import random
import os
import sys


# Nombre màxim de cops que es cridarà al sistema. Si està repetit no el crearà
NombreACrear = 10 

parametres = [
    ['Fnum', 2**10],
    ['block', 8], #1
    ['alg', 4],
    ['FB', 8], #3
    ['KSR', 2],
    ['MULT', 16],
    ['TL', 64], #6
    ['VIB', 2],
    ['AR', 16],
    ['AM', 2], #9
    ['KSL', 4],
    ['DR', 16], #11
    ['EGT', 2],
    ['SL', 16],
    ['RR', 16],
    ['WS', 8],
    ['KSR-6', 2], #16
    ['MULT-6', 16],
    ['TL-6', 64],
    ['VIB-6', 2],
    ['AR-6', 16],
    ['AM-6', 2], #21
    ['KSL-6', 4],
    ['DR-6', 16],
    ['EGT-6', 2],
    ['SL-6', 16],
    ['RR-6', 16], #26
    ['WS-6', 8],
    ['KSR-3', 2],
    ['MULT-3', 16],
    ['TL-3', 64],
    ['VIB-3', 2], #31
    ['AR-3', 16],
    ['AM-3', 2],
    ['KSL-3', 4],
    ['DR-3', 16],
    ['EGT-3', 2], #36
    ['SL-3', 16],
    ['RR-3', 16],
    ['WS-3', 8],
    ['KSR-9', 2],
    ['MULT-9', 16], #41
    ['TL-9', 64],
    ['VIB-9', 2],
    ['AR-9', 16],
    ['AM-9', 2],
    ['KSL-9', 4], #46
    ['DR-9', 16],
    ['EGT-9', 2],
    ['SL-9', 16],
    ['RR-9', 16],
    ['WS-9', 8], #51
]

if __name__ == '__main__':
    # random.seed(1234)
    creats = 0
    for k in range(NombreACrear):
        comanda = ['./configureOPL3_generateFM']

        for parametre in parametres:
            comanda.append(str(random.randint(0,parametre[1]-1)))

        # Mirem si ja existeix
        # Remove the first element (the command)
        arguments = comanda[2:]

        # Format the arguments into the desired string
        formatted_string = "_".join(f"{int(arg):03d}" for arg in arguments)
        formatted_string = f"{int(comanda[1]):05d}_" + formatted_string 

        # Check if any file in the directory contains the formatted string in its name
        current_directory = os.getcwd()
        trobat = False
        for fitxer in os.listdir(current_directory):
            if formatted_string in fitxer:
                trobat = True
                break

        if not trobat:
            subprocess.run(comanda)
            creats = creats + 1


    print(f"S'han creat {creats} tons")

# He de provar que dels generats amb totes les combinacions crear una taula amb la meva mesura de discrepància i avaluar si és un bon criteri o no
