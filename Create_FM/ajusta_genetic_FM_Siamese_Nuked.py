# Intentarem crear un algoritme que ajusta els diferents paràmetres de l'OPL3 quan se li passa un wav extern
# Està basta en ajusta_genetic_FM, però utilitzarem la generació del Nuked i una xarxa neuronal siamesa per comparar si dos wav són semblants
# El .c generador, ara només fa una mostra d'un segon a 44100 Hz. Els fitxers wavs hauran d'estar limitats a això
import numpy as np
from scipy.io import wavfile
import subprocess
import pygad
import pdb
import pickle
import os
import time
import sys
import torch
import torchvision.models as models
import torch.nn as nn
import torchaudio

parametres = [
    ['Fnum', 0xA0, 0xFFFF, -1, np.array([363,385,408,432,458,485,514,544,577,611,647,686])], # He posat -1 perquè té una programació diferent dels registres el F-number. He posat aquests valors, per coincidir amb els del tracker, però depèn per què, es podrien fer tots els valors possibles, l'únic que tardaria més l'algoritme
    ['block', 0xB0, 7, 2, np.arange(8)], #1
    ['alg', 0xC0, 3, -1, np.arange(4)],
    ['FB', 0xC0, 7, 1, np.arange(8)], #3
    ['KSR', 0x20, 1, 4, np.arange(2)],
    ['MULT', 0x20, 15, 0, np.arange(16)],
    ['TL', 0x40, 0x3F, 0, np.arange(64)], #6
    ['VIB', 0x20, 1, 6, np.arange(2)],
    ['AR', 0x60, 0x0F, 4, np.arange(16)],
    ['AM', 0x20, 1, 7, np.arange(2)], #9
    ['KSL', 0x40, 3, 6, np.arange(4)],
    ['DR', 0x60, 0x0F, 0, np.arange(16)], #11
    ['EGT', 0x20, 1, 5, np.arange(2)],
    ['SL', 0x80, 15, 4, np.arange(16)],
    ['RR', 0x80, 15, 0, np.arange(16)],
    ['WS', 0xE0, 7, 0, np.arange(8)],
    ['KSR-6', 0x26, 1, 4, np.arange(2)], #16
    ['MULT-6', 0x26, 15, 0, np.arange(16)],
    ['TL-6', 0x46, 0x3F, 0, np.arange(64)],
    ['VIB-6', 0x26, 1, 6, np.arange(2)],
    ['AR-6', 0x66, 0x0F, 4, np.arange(16)],
    ['AM-6', 0x26, 1, 7, np.arange(2)], #21
    ['KSL-6', 0x46, 3, 6, np.arange(4)],
    ['DR-6', 0x66, 15, 0, np.arange(16)],
    ['EGT-6', 0x26, 1, 5, np.arange(2)],
    ['SL-6', 0x86, 15, 4, np.arange(16)],
    ['RR-6', 0x86, 15, 0, np.arange(16)], #26
    ['WS-6', 0xE0, 7, 0, np.arange(8)],
    ['KSR-3', 0x20, 1, 4, np.arange(2)],
    ['MULT-3', 0x20, 15, 0, np.arange(16)],
    ['TL-3', 0x40, 0x3F, 0, np.arange(64)],
    ['VIB-3', 0x20, 1, 6, np.arange(2)], #31
    ['AR-3', 0x60, 0x0F, 4, np.arange(16)],
    ['AM-3', 0x20, 1, 7, np.arange(2)],
    ['KSL-3', 0x40, 3, 6, np.arange(4)],
    ['DR-3', 0x60, 15, 0, np.arange(16)],
    ['EGT-3', 0x20, 1, 5, np.arange(2)], #36
    ['SL-3', 0x80, 15, 4, np.arange(16)],
    ['RR-3', 0x80, 15, 0, np.arange(16)],
    ['WS-3', 0xE0, 7, 0, np.arange(8)],
    ['KSR-9', 0x20, 1, 4, np.arange(2)],
    ['MULT-9', 0x20, 15, 0, np.arange(16)], #41
    ['TL-9', 0x40, 0x3F, 0, np.arange(64)],
    ['VIB-9', 0x20, 1, 6, np.arange(2)],
    ['AR-9', 0x60, 0x0F, 4, np.arange(16)],
    ['AM-9', 0x20, 1, 7, np.arange(2)],
    ['KSL-9', 0x40, 3, 6, np.arange(4)], #46
    ['DR-9', 0x60, 15, 0, np.arange(16)],
    ['EGT-9', 0x20, 1, 5, np.arange(2)],
    ['SL-9', 0x80, 127, 4, np.arange(16)],
    ['RR-9', 0x80, 127, 0, np.arange(16)],
    ['WS-9', 0xE0, 7, 0, np.arange(8)], #51
]
# Hi ha dos paràmetres més el DVB i DAM, que no surten a la part gràfica del furnace 6.3. En el discord diu que són global parameters, són de tot el xip, no va per instrument, per això no haurien de sortir al fui. Els del fui són per l'ESFM
espai_gens = [row[4] for row in parametres]

# En total tenim de combinacions 12*8*4*8*(2*16*64*2*16*2*4*16*2*16*16*8)^4 = 105553116266500 ~= 10^14
# Si féssim només 2op seria 12*8*4*8*(2*16*64*2*16*2*4*16*2*16*16*8)^2 = 105553116266498 ~= 10^14

# Els instruments que hi ha de 4op al furnace els utilitzo com a població inicial
poblacio_inicial_instruments_furnace = [
[485, 3, 0, 7,1,0,63,0,15,0,0,15,0,15,15,0,1,1,27,0,15,1,0,0,0,15,15,0,0,2,20,0,15,0,0,0,0,15,4,3,0,1,2,0,15,0,0,0,0,15,9,6,], #instruments/OPL/Psuedo-PoKEY Perodic Noise (4OP).fui
[485, 3, 0, 4,1,0,26,0,12,0,0,11,0,12,11,0,1,5,0,1,7,0,0,3,0,11,10,6,0,5,18,0,11,1,0,7,0,15,12,0,0,1,2,0,9,0,0,4,0,7,10,0,], #instruments/OPL/OPL3-TwinkleKey.fui
[485, 3, 2, 5,1,1,23,0,15,0,0,5,0,12,9,0,1,0,21,0,13,0,0,3,0,12,9,0,0,9,38,0,9,0,0,6,0,13,9,0,0,0,0,0,13,0,0,3,0,15,10,0,], #instruments/OPL/OPL3_Springybass.fui
[485, 3, 0, 7,1,10,40,0,15,0,0,7,0,2,6,0,1,0,26,0,15,0,0,5,0,6,5,0,0,0,32,0,14,0,0,6,0,3,4,0,0,1,0,0,15,0,0,5,0,1,9,0,], #instruments/OPL/OPL3_Slap_Bass.fui
[485, 3, 0, 0,1,0,0,0,15,1,0,0,0,0,15,0,1,0,0,0,15,0,0,0,0,0,15,0,0,0,0,0,15,0,0,0,0,15,15,0,0,0,0,0,11,0,0,5,0,2,7,6,], #instruments/OPL/OPL3-PeriodicNoise.fui
[485, 3, 2, 0,1,1,0,0,9,0,0,5,0,2,15,2,1,1,35,0,11,0,0,8,0,3,15,0,0,5,14,0,12,1,0,5,0,4,15,0,0,1,0,0,7,0,0,2,0,0,15,0,], #instruments/OPL/OPL3-FakeFalcomString.fui
[485, 3, 0, 6,1,3,24,0,8,1,1,3,0,2,7,0,1,1,25,0,9,0,0,5,0,1,6,0,0,1,24,1,7,0,0,2,0,1,7,0,0,1,0,0,11,0,0,0,0,15,8,0,], #instruments/OPL/OPL3-FakeFalcomGuitar.fui
[485, 3, 0, 0,1,0,0,0,15,1,0,0,0,0,0,0,1,0,0,0,15,0,0,0,0,0,0,0,0,0,0,0,15,0,0,0,0,15,0,0,0,0,0,0,11,0,0,6,0,3,7,6,], #instruments/OPL/OPL3-clap.fui
[485, 3, 2, 0,1,0,0,0,15,0,0,5,0,3,8,0,1,0,26,0,15,0,0,6,0,2,7,0,0,4,26,1,8,1,0,7,0,9,9,0,0,1,0,0,15,0,0,2,0,15,11,0,], #instruments/OPL/OPL3_BigBass.fui
[485, 3, 0, 5,1,6,28,0,15,0,0,4,0,4,6,0,1,0,20,0,15,0,0,4,0,3,6,0,0,5,45,0,15,0,0,6,0,4,6,0,0,1,6,0,15,0,0,2,0,15,8,0,], #instruments/OPL/opl3_4op_bass_1.fui
[485, 3, 4, 3,1,1,27,0,15,0,2,1,0,15,5,0,1,10,41,0,15,0,0,2,0,6,5,0,0,1,2,0,15,0,0,3,0,15,7,0,0,1,2,0,15,0,0,3,0,15,7,0,], #instruments/OPL/DX7 Electric Piano.fui
[485, 3, 6, 0,1,5,13,1,9,0,1,6,0,7,5,4,1,2,19,0,7,0,0,3,0,2,4,0,0,4,41,1,15,0,0,0,0,15,4,2,0,2,2,1,15,0,0,3,0,15,5,0,], #instruments/OPL/Crystal.fui
[485, 3, 4, 4,1,0,39,1,5,0,0,0,0,0,0,2,1,5,52,0,5,0,0,0,0,4,5,4,0,1,9,0,5,0,0,4,0,5,4,1,0,1,2,0,5,0,0,0,0,5,4,0,], #instruments/OPL/Chorus Organ.fui
[485, 3, 0, 0,1,12,21,1,15,0,2,6,0,3,0,0,1,0,20,0,15,0,0,9,0,2,0,0,0,5,40,1,15,0,0,8,0,2,6,0,0,2,3,0,15,0,0,2,0,15,6,0,], #instruments/OPL/4op Bass.fui
[485, 3, 6, 0,1,5,0,0,15,0,0,4,0,15,7,6,1,4,0,0,15,0,0,3,0,12,5,6,0,7,4,0,15,0,0,2,0,14,3,6,0,6,0,0,15,0,0,4,0,15,7,6,], #instruments/OPL/OPL3-MajorSquare.fui
[485, 3, 0, 7,1,2,4,0,15,0,0,7,0,3,6,1,1,1,12,0,15,1,0,6,0,2,6,1,0,0,12,1,15,0,0,3,0,3,6,0,0,1,10,0,15,0,0,1,0,15,6,0,], #instruments/OPL/opl3_4op_rhythm_guitar_2.fui
[485, 3, 0, 7,1,2,4,0,15,0,0,7,0,3,6,1,1,0,12,0,15,1,0,6,0,2,6,3,0,0,12,1,15,0,0,3,0,3,6,0,0,1,10,0,15,0,0,1,0,15,6,0,], #instruments/OPL/opl3_4op_rhythm_guitar_1.fui
[485, 3, 4, 6,0,3,25,0,13,0,1,1,0,15,5,0,0,14,32,0,15,0,1,2,0,14,5,0,0,1,4,1,15,0,2,1,0,15,5,0,0,4,12,0,15,1,0,3,0,15,5,0,], #instruments/OPL/opl3_4op_piano_bell.fui
[485, 3, 0, 6,1,2,5,0,15,0,0,8,0,10,12,0,1,0,6,0,15,1,0,3,0,3,7,2,0,2,35,1,15,0,0,3,0,6,10,1,0,1,10,1,15,0,0,0,0,0,6,0,], #instruments/OPL/opl3_4op_lead_guitar.fui
[485, 3, 0, 7,1,5,34,0,15,0,0,2,0,9,15,1,1,0,18,0,15,1,0,0,0,0,15,0,0,2,28,1,4,0,0,3,0,15,15,2,0,0,10,1,7,0,0,0,0,15,6,0,], #instruments/OPL/opl3_4op_growl_synth.fui
[485, 3, 4, 7,1,8,0,0,15,0,0,5,0,10,0,0,1,2,15,0,15,0,0,9,0,15,6,7,0,2,6,0,15,0,0,6,0,10,10,0,0,1,0,0,15,0,0,0,0,0,0,0,], #instruments/OPL/opl3_4op_drum_tom.fui
[485, 3, 0, 7,1,4,0,0,15,0,0,3,0,4,0,0,1,2,0,0,15,0,0,10,0,9,6,0,0,2,8,0,15,0,0,6,0,7,10,0,0,1,0,0,15,0,0,0,0,0,0,0,], #instruments/OPL/opl3_4op_drum_snare.fui
[485, 3, 0, 3,1,3,0,0,15,0,1,3,0,2,7,2,1,1,0,0,15,0,0,8,0,1,15,2,0,2,0,0,15,0,0,4,0,8,4,6,0,0,4,0,15,0,0,5,0,15,9,6,], #instruments/OPL/opl3_4op_drum_power_snare.fui
[485, 3, 0, 7,1,4,0,0,15,0,0,5,0,8,0,0,1,2,0,0,15,0,0,11,0,13,6,0,0,2,8,0,15,0,0,8,0,11,10,0,0,1,0,0,15,0,0,0,0,0,0,0,], #instruments/OPL/opl3_4op_drum_kick.fui
[485, 3, 0, 0,1,5,32,0,2,1,0,3,0,11,4,0,1,3,18,0,9,1,0,1,0,15,5,0,0,0,23,0,15,1,0,4,0,5,4,0,0,1,0,0,15,0,0,5,0,1,5,0,], #instruments/OPL/OPL3-4opsitar.fui
[485, 3, 0, 0,1,9,25,0,5,0,0,2,0,5,10,0,1,1,26,0,15,0,0,2,0,6,7,0,0,6,41,0,12,0,0,1,0,7,7,0,0,1,0,0,6,0,0,2,0,1,13,0,], #instruments/OPL/OPL3-4opharmonica.fui
[485, 3, 0, 0,1,1,12,0,8,0,1,7,0,4,15,0,1,1,12,1,6,1,0,5,0,1,15,1,0,7,48,0,7,0,0,5,0,2,15,0,0,1,0,0,8,0,0,15,0,0,15,0,], #instruments/OPL/OPL3-4opbrass11.fui
[485, 3, 4, 4,0,2,22,1,15,0,0,0,0,0,0,0,0,2,22,0,15,1,2,0,0,0,0,0,0,2,10,1,7,0,0,6,0,1,6,0,0,2,10,0,7,1,1,6,0,1,4,0,], #instruments/OPL/opl3_4op_strings.fui
[485, 3, 4, 6,0,3,25,0,13,0,1,1,0,15,5,0,0,14,32,0,15,0,1,2,0,14,5,0,0,1,4,1,15,0,2,1,0,15,5,0,0,4,12,0,15,1,0,3,0,15,5,0,], #instruments/OPL/pianoBell.fui    
]

# poblacio_inicial = []
# for freq in parametres[0][4]:
#     for bloc in parametres[1][4]:
#         for instrument in poblacio_inicial_instruments_furnace:
#             nou_instru = instrument.copy()
#             nou_instru[0] = freq
#             nou_instru[1] = bloc
#             poblacio_inicial.append(nou_instru)
poblacio_inicial = poblacio_inicial_instruments_furnace

# Carreguem la xarxa neuronal

# Define the same model architecture
class SiameseNetworkResNet(nn.Module):
    def __init__(self):
        super(SiameseNetworkResNet, self).__init__()
        resnet = models.resnet18(pretrained=False)  # No pretraining needed during inference
        resnet.conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.feature_extractor = nn.Sequential(*list(resnet.children())[:-1])  # Remove last FC layer
        self.fc = nn.Linear(resnet.fc.in_features, 256)  # Same FC layer as before

    def forward_once(self, x):
        x = self.feature_extractor(x)
        x = torch.flatten(x, 1)
        x = self.fc(x)
        return x

    def forward(self, input1, input2):
        output1 = self.forward_once(input1)
        output2 = self.forward_once(input2)
        return output1, output2

# Initialize model and load the trained weights
model = SiameseNetworkResNet()
model.load_state_dict(torch.load("siamese_model.pth"))
model.eval()  # Set the model to evaluation mode

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model.to(device)

# Function to convert WAV to Mel spectrogram
def audio_to_mel(filepath, sr=44100, n_fft=1024, hop_length=512, n_mels=64):
    waveform, sample_rate = torchaudio.load(filepath)
    waveform = waveform / waveform.abs().max()

    # Convert to mono if the audio has multiple channels
    if waveform.shape[0] > 1:
        waveform = torch.mean(waveform, dim=0, keepdim=True)

    # Resample if needed
    if sample_rate != sr:
        resampler = torchaudio.transforms.Resample(orig_freq=sample_rate, new_freq=sr)
        waveform = resampler(waveform)

    # Convert to Mel spectrogram
    mel_spec_transform = torchaudio.transforms.MelSpectrogram(
        sample_rate=sr, n_fft=n_fft, hop_length=hop_length, n_mels=n_mels
    )
    mel_spec = mel_spec_transform(waveform)

    # Normalize and add batch dimension
    mel_spec = torch.log(mel_spec + 1e-9)  # Avoid log(0)
    mel_spec = mel_spec.unsqueeze(0)  # Add channel dimension

    return mel_spec.to(device)

def compare_audio(file1, file2):
    # Convert audio files to mel spectrograms
    input1 = audio_to_mel(file1)
    input2 = audio_to_mel(file2)

    with torch.no_grad():
        # Get embeddings from the model
        embedding1 = model.forward_once(input1)
        embedding2 = model.forward_once(input2)

        # Compute similarity (Euclidean distance)
        distance = torch.nn.functional.pairwise_distance(embedding1, embedding2)

    return distance.item()


# Path to the WAV file
# Ha de ser un wav de 1s a 44100 sampling rate
solution_wav_file_path = "/home/jbd/Documents/JBD/furnace/BreakingGlass.wav"
# Remostregem perquè sempre sigui la mateixa longitud de 44100
waveform, sr = torchaudio.load(solution_wav_file_path)
current_length = waveform.shape[-1]
resampler = torchaudio.transforms.Resample(orig_freq=current_length, new_freq=44100)
resampled_wave = resampler(waveform)
solution_wav_file_path = "resampled_wave.wav"
torchaudio.save(solution_wav_file_path,resampled_wave, 44100)

# Track the number of evaluations within the current generation
fitness_eval_counter = 0
total_evaluations_per_generation = None  # To be calculated once

def fitness_func(ga_instance, solution, solution_idx): 
    # Per provar he fet aquesta solució, però les comandes del 0x60 surten en 7 bits en lloc de 8. Són les solutions[8] i [11]
    #  solution = [ 647,4,3,6, 1, 7,59,1,6,0, 3,7,1,7,6, 3, 0, 6,49,1,4,0, 2,1,1,4,3, 2, 0, 4,39,1,6,1, 3,5,1,5,6, 4, 1, 3,29,1,4,0, 3,4,1,5,7, 4]

    # solution = [647,3,2,4,0, 2,22,1,15,0, 0,0,1,0,0, 0,0,2,22,0, 15,1,2,0,1, 0,0,0,0,2, 10,1,7,0,1, 6,1,1,6,0, 0,2,10,0,7, 1,2,6,1,1, 4,0] # La solució de 4op-strings

    global fitness_eval_counter, total_evaluations_per_generation

    # Initialize total evaluations on the first call
    if total_evaluations_per_generation is None:
        total_evaluations_per_generation = len(ga_instance.population)

    fitness_eval_counter += 1  # Increment the counter

    # Print progress every 10 evaluations (adjust as needed)
    if fitness_eval_counter % 10 == 0 or fitness_eval_counter == total_evaluations_per_generation:
        print(f"Fitness evaluation {fitness_eval_counter}/{total_evaluations_per_generation} for generation {ga_instance.generations_completed + 1}")

    solution = solution.astype(int)
    # Amb els paràmetres d'aquesta solució he de crear el so amb el Nuked, comparar-ho amb el resultat ideal que es vol aconseguir i enviar la mesura de distància obtinguda per aquest codi genètic.
    # El fitness del genetic algorithm espera que el 0 és una pitjor adaptació i que com més alt millor. El resultat del siamese

    # Generem wav amb els paràmetres de solution
    comanda = ['./configureOPL3_generateFM']
    for parametre in solution:
        comanda.append(str(parametre))
    subprocess.run(comanda, stdout=subprocess.DEVNULL) # No vull que aparegui cap missatge

    # Creem el nom del fitxer per llegir-lo torchaudio
    nomFitxer = f"to_{solution[0]:05d}"
    for parametre in solution[1:]:
        nomFitxer = nomFitxer + f"_{parametre:03d}"
    nomFitxer = nomFitxer + ".wav"

    similarity = compare_audio(nomFitxer, solution_wav_file_path)
    # print(f"similarity: {similarity} -.- Nom_fitx: {nomFitxer}")
    try:
        # Hem d'invertir la semblança per a fitness, molt bon fitness és que són molt semblants i per la xarxa neuronal això és 0
        fitness = 1 / (similarity + 1e-6)
    except Exception:
        fitness = 0

    return fitness


def on_generation_jep(ga_instance):
    global start_time, fitness_eval_counter
    fitness_eval_counter = 0
    
    end_time = time.time()
    elapsed_time = end_time - start_time

    print(f"Generation = {ga_instance.generations_completed}")
    print(f"Fitness of the best solution = {ga_instance.best_solution()[1]}")
    print(f"Best solution index: {ga_instance.best_solution()}")
    print(f"Average Fitness = {np.mean(ga_instance.last_generation_fitness)}")
    print(f"Diversity = {len(np.unique(ga_instance.population, axis=0))}")
    print(f"Time passed in this generation = {elapsed_time:.4f} seconds")
    start_time = time.time()

    # Save the state every 20 generations
    if ga_instance.generations_completed % 20 == 0:
        save_state(ga_instance, f'ga_instance_state_{ga_instance.generations_completed}.pkl')
    

def save_state(ga_instance, filename):
    with open(filename, 'wb') as file:
        pickle.dump(ga_instance, file)
        print(f"State saved at generation {ga_instance.generations_completed}")

def load_state(filename):
    with open(filename, 'rb') as file:
        ga_instance = pickle.load(file)
        print(f"State loaded from generation {ga_instance.generations_completed}")
        return ga_instance

# inicialitzem el time
start_time = time.time()

# Check if there is a saved state to load
if os.path.exists('ga_instance_state_21.pkl'):
    print("*** Carreguem l'estat que ja teníem")
    ga_instance = load_state('ga_instance_state_20.pkl')
else:
    ga_instance = pygad.GA(
        num_generations = 10,
        num_parents_mating = int(len(poblacio_inicial)/2),
        initial_population = poblacio_inicial,
        num_genes = len(espai_gens), # són el nombre de paràmetres
        mutation_type = "adaptive",
        mutation_probability = [0.4,0.1], # Un 40% que mutin dels que no tenen bons gens i un 10% dels gens muten de la nova generació
        fitness_func = fitness_func,
        stop_criteria = ["reach_5.60", "saturate_5"], # Hi ha saturació o reach, saturació és que es repeteixi i reach que s'acosti a l'òptim, però es poden combinar els dos
        gene_space = espai_gens, #he de donar el valor dels paràlmetres que poden prendre
        on_generation = on_generation_jep,
        parent_selection_type = "tournament",
        keep_elitism = int(len(poblacio_inicial)/4) # Guardem una proporció dels millors
    )


try:
    ga_instance.run()
except Exception as e:
    # Peta perquè hi ha un nan en els fitness
    print(f"IndexError encountered: {e}")
    print("Checking the population fitness array and other parameters.")
    pop_fitness = ga_instance.last_generation_fitness
    print("Population Fitness: ", pop_fitness)
    if len(pop_fitness) == 0:
        raise ValueError("Population fitness array is empty. Check the fitness function and initial population.")


best_solution, best_solution_fitness, best_match_idx = ga_instance.best_solution()
print(f"Trobada {best_solution} - {best_solution_fitness} - TLs: {best_solution[6]}, {best_solution[18]}, {best_solution[30]}, {best_solution[42]}")
# Creem el nom del fitxer per llegir-lo torchaudio
nomFitxer_opt = f"to_{int(best_solution[0]):05d}"
for parametre in best_solution[1:]:
    nomFitxer_opt = nomFitxer_opt + f"_{int(parametre):03d}"
print(f"{nomFitxer_opt}.wav")

print("\U0001F389\U0001F389\U0001F389 Hem acabat")
