#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "opl3.h"

#define SAMPLE_RATE 44100
#define DURATION_SECONDS 3

// WAV header structure
typedef struct {
    char chunkID[4];        // "RIFF"
    uint32_t chunkSize;     // Size of the file minus 8 bytes
    char format[4];         // "WAVE"
    char subchunk1ID[4];    // "fmt "
    uint32_t subchunk1Size; // Size of subchunk1 (16 for PCM)
    uint16_t audioFormat;   // Audio format (1 for PCM)
    uint16_t numChannels;   // Number of channels
    uint32_t sampleRate;    // Sample rate
    uint32_t byteRate;      // Byte rate
    uint16_t blockAlign;    // Block align
    uint16_t bitsPerSample; // Bits per sample
    char subchunk2ID[4];    // "data"
    uint32_t subchunk2Size; // Size of subchunk2 (numSamples * numChannels * bitsPerSample/8)
} WAVHeader;

// Write a WAV file
void write_wav(const char *filename, int16_t *samples, uint32_t num_samples, uint32_t sample_rate) {
    WAVHeader header;
    FILE *file = fopen(filename, "wb");

    if (!file) {
        perror("Failed to open file");
        return;
    }

    // Fill in WAV header
    memcpy(header.chunkID, "RIFF", 4);
    header.chunkSize = 36 + num_samples * sizeof(int16_t);
    memcpy(header.format, "WAVE", 4);
    memcpy(header.subchunk1ID, "fmt ", 4);
    header.subchunk1Size = 16;
    header.audioFormat = 1;
    header.numChannels = 1;
    header.sampleRate = sample_rate;
    header.byteRate = sample_rate * sizeof(int16_t);
    header.blockAlign = sizeof(int16_t);
    header.bitsPerSample = 16;
    memcpy(header.subchunk2ID, "data", 4);
    header.subchunk2Size = num_samples * sizeof(int16_t);

    // Write header and samples
    fwrite(&header, sizeof(WAVHeader), 1, file);
    fwrite(samples, sizeof(int16_t), num_samples, file);

    fclose(file);
}

// Configure OPL3 for a C-3 tone
void configure_c3_tone(opl3_chip *chip) {
    uint16_t fnum = 0x200; // FNUM for C-3
    uint8_t block = 4;     // Block value for octave 3

    // Write registers to configure tone
    OPL3_WriteReg(chip, 0xA0, fnum & 0xFF);     // Low 8 bits of FNUM
    OPL3_WriteReg(chip, 0xB0, ((fnum >> 8) & 0x03) | (block << 2) | 0x20); // Block and key-on
    OPL3_WriteReg(chip, 0x20, 0x01); // Set operator parameters (e.g., enable tone)
}

int main() {
    opl3_chip chip;
    uint32_t num_samples = SAMPLE_RATE * DURATION_SECONDS*2; // ja que genera en estereo
    int16_t *samples = (int16_t *)calloc(num_samples, sizeof(int16_t));
    uint32_t i;

    if (!samples) {
        fprintf(stderr, "Failed to allocate memory for samples.\n");
        return 1;
    }

    // Initialize OPL3 chip
    OPL3_Reset(&chip, SAMPLE_RATE);

    // Configure the chip for a C-3 tone
    configure_c3_tone(&chip);

    // Generate audio samples
    OPL3_GenerateStream(&chip, samples, num_samples);

    /* // Write to WAV file */
    /* write_wav("C3_tone.wav", samples, num_samples, SAMPLE_RATE); */

    // Free memory
    free(samples);

    printf("Generated C-3 tone and saved to 'C3_tone.wav'.\n");
    return 0;
}
