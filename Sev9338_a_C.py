# Script que agafa un fitxer en JSON del projecte de SEV9338 i el passa a text per poder ser introduït al C
import json
import os

# Load the JSON file
file_path = "/home/jepsuse/MSX/Sprites/Bricks.scumsx2"
directory = os.path.dirname(file_path)
filename_with_extension = os.path.basename(file_path)
filename, extension = os.path.splitext(filename_with_extension)
fitxer_gravar = directory + '/' + filename + '_sprites.h'

with open(file_path, 'r') as file:
    data = json.load(file)

# Process palettes
palettes = {}
for palette_name, colors in data['palettes']:
    processed_palette = []
    for color in colors:
        r = (color >> 12) & 0xF  # Extract the R component (4 bits)
        g = (color) & 0xF   # Extract the G component (4 bits)
        b = (color >> 8) & 0xF   # Extract the B component (4 bits)
        processed_palette.append([r, g, b])
    palettes[palette_name] = processed_palette

# Process sprites
sprites = {
    'bitmap': [],
    'mask': [],
    'colors': []
}

for sprite in data['sprites']:
    bitmap, mask, colors = sprite
    sprites['bitmap'].append(bitmap)
    sprites['mask'].append(mask)
    sprites['colors'].append(colors)

# Combine the processed data
processed_data = {
    'palettes': palettes,
    'sprites': sprites
}

with open(fitxer_gravar, 'w') as fw:
    comptador = 0
    for sprite in sprites['bitmap']:
        fw.write(f"//{comptador}\n")
        # Els valors estan transposats, primer creem la matriu de 16 i després la transposem
        matrix = [sprite[i:i+16] for i in range(0, len(sprite), 16)]
        transposed_matrix = zip(*matrix) # El * desempaqueta la matriu i el zip torna a agafar un element de cada vector desempaquetat per l'operació *
        for row in transposed_matrix:
            # Print 16 elements at a time, separated by spaces
            line = ''.join(map(str, row))
            fw.write('0b' + line + ',\n')
        comptador += 1

    fw.write("\n/*    MASKS     */\n")
    comptador = 0
    for mask in sprites['mask']:
        fw.write(f"//{comptador}\n{{")
        for i in range(0, len(mask), 4):
            line = ','.join(map(str, [int(value) for value in mask[i:i+4]]))
            fw.write(line + ', ')
        comptador += 1
        fw.write("},\n")

    fw.write("\n/*    Index paletes     */\n")
    comptador = 0
    for color in sprites['colors']:
        fw.write(f"//{comptador}\n{{")
        for i in range(0, len(color), 4):
            line = ','.join(map(str, color[i:i+4]))
            fw.write(line + ', ')
        comptador += 1
        fw.write("},\n")

    fw.write("\n/* Paleta de colors */\n")
    for key, paleta in palettes.items():
        fw.write(f'char {key}[] = [\n')
        for index, color in enumerate(paleta):
            fw.write(f"{index}, {','.join(str(x) for x in color),}\n")

    # Ara he d'agafar el index i si fa overlapping i fusionar-ho per ser un array. Potser l'altre no faria falta
    # Masks i index palettes fusionat
    fw.write("\n\n ***********************\n *** Mask i index fusionats\n ***********************\nchar index_mask[] = {")
    comptador = 0
    for masks_colors in zip(sprites['mask'],sprites['colors']):
        fw.write(f"//{comptador}\n")
        mask, color = masks_colors
        for mask_color in zip(mask,color):
            fw.write(f"{mask_color[0]<<6 | mask_color[1]},")
        comptador += 1
        fw.write(",\n")
    fw.write("};\n")
