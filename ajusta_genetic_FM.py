# Intentarem crear un algoritme que ajusta els diferents paràmetres de l'OPL3 quan se li passa un wav extern
# Primer provarem d'un so ja sintètic del vgm2wav en el qual només anem canviant un paràmetre
# Cada cop que canviem un paràmetre, modifiquem la configuració de l'OPL3(OPL4) i tornem a generar el wav amb el vgm2wav
# El genetic algorithm ha funcionat. Primer provarem d'ajustar el .wav a una sola nota i canal de 4 operacions. Després podrem fer a 2 operacions. I després durant més d'una nota, per fer tota la síntesi de la veu. S'ha d'anar tallant el .wav
# També començarem que la nota no s'estima trobant. Quan faig només una nota és per estimar l'instrument que soni per exemple com el cant d'un ocell
# El que també faltarà és averiguar com tradueixo la longitud d'un wav qualsevol al temps de les notes tocades al vgm
# El vgm2wav generar els wav a 44100 mostres per segon. He d'estimar la comanda pausa per poder ajustar bé els temps i poder compararbñe les formes d'ona.
# L'ordre dins de la solució és sempre el mateix, per tant tindrem un array a on cada posició té el nom del paràmetre, el número de registre a canviar dins el vgm file, valor per fer and, valor de bits a desplaçar esquerra i un array amb els possibles valors que pot prendre el paràmetre
# Començarem només amb un canal, per tant farem el canal 0 format pels slots 0,6,3,9. Ja posats en l'ordre que es configuren en el fui. Però aquí és independent ja que escrivim els valors directament al fitxer
# PArtiré d'un fitxer capçalera vgm mínim per opl4 d'un canal que aniré ampliant amb els valors dels paràmetres
import numpy as np
from scipy.io import wavfile
import subprocess
import pygad
import pdb
import pickle
import os
import time
import sys
sys.path.append('/home/jbd/audio-similarity/audio_similarity')
from audio_similarity import AudioSimilarity


# Hauria de provar amb librosa per fer les extraccions dels features i mirar que realment el senyal s'està generant bé.

# 0x1e6 és tot després dels que ja estan posats a 0 i a oon comença la configuració. No sé com l'offset del 0x34 es tradueix al 0x111. El fitxer capçalera li he dit '/home/jbd/Documents/JBD/furnace/fitx_heading_OPL4.vgm'


parametres = [
    ['Fnum', 0xA0, 0xFFFF, -1, np.array([363,385,408,432,458,485,514,544,577,611,647,686])], # He posat -1 perquè té una programació diferent dels registres el F-number. He posat aquests valors, per coincidir amb els del tracker, però depèn per què, es podrient fer tots els valors possibles, l'únic que tardaria més l'algoritme
    ['block', 0xB0, 7, 2, np.arange(8)], #1
    ['alg', 0xC0, 3, -1, np.arange(4)],
    ['FB', 0xC0, 7, 1, np.arange(8)], #3
    ['KSR', 0x20, 1, 4, np.arange(2)],
    ['MULT', 0x20, 15, 0, np.arange(16)],
    ['TL', 0x40, 0x3F, 0, np.arange(64)], #6
    ['VIB', 0x20, 1, 6, np.arange(2)],
    ['AR', 0x60, 0x0F, 4, np.arange(16)],
    ['AM', 0x20, 1, 7, np.arange(2)], #9
    ['KSL', 0x40, 3, 6, np.arange(4)],
    ['DR', 0x60, 0x0F, 0, np.arange(16)], #11
    ['EGT', 0x20, 1, 5, np.arange(2)],
    ['SL', 0x80, 15, 4, np.arange(16)],
    ['RR', 0x80, 15, 0, np.arange(16)],
    ['WS', 0xE0, 7, 0, np.arange(8)],
    ['KSR-6', 0x26, 1, 4, np.arange(2)], #16
    ['MULT-6', 0x26, 15, 0, np.arange(16)],
    ['TL-6', 0x46, 0x3F, 0, np.arange(64)],
    ['VIB-6', 0x26, 1, 6, np.arange(2)],
    ['AR-6', 0x66, 0x0F, 4, np.arange(16)],
    ['AM-6', 0x26, 1, 7, np.arange(2)], #21
    ['KSL-6', 0x46, 3, 6, np.arange(4)],
    ['DR-6', 0x66, 15, 0, np.arange(16)],
    ['EGT-6', 0x26, 1, 5, np.arange(2)],
    ['SL-6', 0x86, 15, 4, np.arange(16)],
    ['RR-6', 0x86, 15, 0, np.arange(16)], #26
    ['WS-6', 0xE0, 7, 0, np.arange(8)],
    ['KSR-3', 0x20, 1, 4, np.arange(2)],
    ['MULT-3', 0x20, 15, 0, np.arange(16)],
    ['TL-3', 0x40, 0x3F, 0, np.arange(64)],
    ['VIB-3', 0x20, 1, 6, np.arange(2)], #31
    ['AR-3', 0x60, 0x0F, 4, np.arange(16)],
    ['AM-3', 0x20, 1, 7, np.arange(2)],
    ['KSL-3', 0x40, 3, 6, np.arange(4)],
    ['DR-3', 0x60, 15, 0, np.arange(16)],
    ['EGT-3', 0x20, 1, 5, np.arange(2)], #36
    ['SL-3', 0x80, 15, 4, np.arange(16)],
    ['RR-3', 0x80, 15, 0, np.arange(16)],
    ['WS-3', 0xE0, 7, 0, np.arange(8)],
    ['KSR-9', 0x20, 1, 4, np.arange(2)],
    ['MULT-9', 0x20, 15, 0, np.arange(16)], #41
    ['TL-9', 0x40, 0x3F, 0, np.arange(64)],
    ['VIB-9', 0x20, 1, 6, np.arange(2)],
    ['AR-9', 0x60, 0x0F, 4, np.arange(16)],
    ['AM-9', 0x20, 1, 7, np.arange(2)],
    ['KSL-9', 0x40, 3, 6, np.arange(4)], #46
    ['DR-9', 0x60, 15, 0, np.arange(16)],
    ['EGT-9', 0x20, 1, 5, np.arange(2)],
    ['SL-9', 0x80, 127, 4, np.arange(16)],
    ['RR-9', 0x80, 127, 0, np.arange(16)],
    ['WS-9', 0xE0, 7, 0, np.arange(8)], #51
]
# Hi ha dos paràmetres més el DVB i DAM, que no surten a la part gràfica del furnace 6.3. En el discord diu que són global parameters, són de tot el xip, no va per instrument, per això no haurien de sortir al fui. Els del fui són per l'ESFM
espai_gens = [row[4] for row in parametres]

# En total tenim de combinacions 12*8*4*8*(2*16*64*2*16*2*4*16*2*16*16*8)^4 = 105553116266500 ~= 10^14
# Si féssim només 2op seria 12*8*4*8*(2*16*64*2*16*2*4*16*2*16*16*8)^2 = 105553116266498 ~= 10^14

# Els instruments que hi ha de 4op al furnace els utilitzo com a població inicial
poblacio_inicial_instruments_furnace = [
[485, 3, 0, 7,1,0,63,0,15,0,0,15,0,15,15,0,1,1,27,0,15,1,0,0,0,15,15,0,0,2,20,0,15,0,0,0,0,15,4,3,0,1,2,0,15,0,0,0,0,15,9,6,], #instruments/OPL/Psuedo-PoKEY Perodic Noise (4OP).fui
[485, 3, 0, 4,1,0,26,0,12,0,0,11,0,12,11,0,1,5,0,1,7,0,0,3,0,11,10,6,0,5,18,0,11,1,0,7,0,15,12,0,0,1,2,0,9,0,0,4,0,7,10,0,], #instruments/OPL/OPL3-TwinkleKey.fui
[485, 3, 2, 5,1,1,23,0,15,0,0,5,0,12,9,0,1,0,21,0,13,0,0,3,0,12,9,0,0,9,38,0,9,0,0,6,0,13,9,0,0,0,0,0,13,0,0,3,0,15,10,0,], #instruments/OPL/OPL3_Springybass.fui
[485, 3, 0, 7,1,10,40,0,15,0,0,7,0,2,6,0,1,0,26,0,15,0,0,5,0,6,5,0,0,0,32,0,14,0,0,6,0,3,4,0,0,1,0,0,15,0,0,5,0,1,9,0,], #instruments/OPL/OPL3_Slap_Bass.fui
[485, 3, 0, 0,1,0,0,0,15,1,0,0,0,0,15,0,1,0,0,0,15,0,0,0,0,0,15,0,0,0,0,0,15,0,0,0,0,15,15,0,0,0,0,0,11,0,0,5,0,2,7,6,], #instruments/OPL/OPL3-PeriodicNoise.fui
[485, 3, 2, 0,1,1,0,0,9,0,0,5,0,2,15,2,1,1,35,0,11,0,0,8,0,3,15,0,0,5,14,0,12,1,0,5,0,4,15,0,0,1,0,0,7,0,0,2,0,0,15,0,], #instruments/OPL/OPL3-FakeFalcomString.fui
[485, 3, 0, 6,1,3,24,0,8,1,1,3,0,2,7,0,1,1,25,0,9,0,0,5,0,1,6,0,0,1,24,1,7,0,0,2,0,1,7,0,0,1,0,0,11,0,0,0,0,15,8,0,], #instruments/OPL/OPL3-FakeFalcomGuitar.fui
[485, 3, 0, 0,1,0,0,0,15,1,0,0,0,0,0,0,1,0,0,0,15,0,0,0,0,0,0,0,0,0,0,0,15,0,0,0,0,15,0,0,0,0,0,0,11,0,0,6,0,3,7,6,], #instruments/OPL/OPL3-clap.fui
[485, 3, 2, 0,1,0,0,0,15,0,0,5,0,3,8,0,1,0,26,0,15,0,0,6,0,2,7,0,0,4,26,1,8,1,0,7,0,9,9,0,0,1,0,0,15,0,0,2,0,15,11,0,], #instruments/OPL/OPL3_BigBass.fui
[485, 3, 0, 5,1,6,28,0,15,0,0,4,0,4,6,0,1,0,20,0,15,0,0,4,0,3,6,0,0,5,45,0,15,0,0,6,0,4,6,0,0,1,6,0,15,0,0,2,0,15,8,0,], #instruments/OPL/opl3_4op_bass_1.fui
[485, 3, 4, 3,1,1,27,0,15,0,2,1,0,15,5,0,1,10,41,0,15,0,0,2,0,6,5,0,0,1,2,0,15,0,0,3,0,15,7,0,0,1,2,0,15,0,0,3,0,15,7,0,], #instruments/OPL/DX7 Electric Piano.fui
[485, 3, 6, 0,1,5,13,1,9,0,1,6,0,7,5,4,1,2,19,0,7,0,0,3,0,2,4,0,0,4,41,1,15,0,0,0,0,15,4,2,0,2,2,1,15,0,0,3,0,15,5,0,], #instruments/OPL/Crystal.fui
[485, 3, 4, 4,1,0,39,1,5,0,0,0,0,0,0,2,1,5,52,0,5,0,0,0,0,4,5,4,0,1,9,0,5,0,0,4,0,5,4,1,0,1,2,0,5,0,0,0,0,5,4,0,], #instruments/OPL/Chorus Organ.fui
[485, 3, 0, 0,1,12,21,1,15,0,2,6,0,3,0,0,1,0,20,0,15,0,0,9,0,2,0,0,0,5,40,1,15,0,0,8,0,2,6,0,0,2,3,0,15,0,0,2,0,15,6,0,], #instruments/OPL/4op Bass.fui
[485, 3, 6, 0,1,5,0,0,15,0,0,4,0,15,7,6,1,4,0,0,15,0,0,3,0,12,5,6,0,7,4,0,15,0,0,2,0,14,3,6,0,6,0,0,15,0,0,4,0,15,7,6,], #instruments/OPL/OPL3-MajorSquare.fui
[485, 3, 0, 7,1,2,4,0,15,0,0,7,0,3,6,1,1,1,12,0,15,1,0,6,0,2,6,1,0,0,12,1,15,0,0,3,0,3,6,0,0,1,10,0,15,0,0,1,0,15,6,0,], #instruments/OPL/opl3_4op_rhythm_guitar_2.fui
[485, 3, 0, 7,1,2,4,0,15,0,0,7,0,3,6,1,1,0,12,0,15,1,0,6,0,2,6,3,0,0,12,1,15,0,0,3,0,3,6,0,0,1,10,0,15,0,0,1,0,15,6,0,], #instruments/OPL/opl3_4op_rhythm_guitar_1.fui
[485, 3, 4, 6,0,3,25,0,13,0,1,1,0,15,5,0,0,14,32,0,15,0,1,2,0,14,5,0,0,1,4,1,15,0,2,1,0,15,5,0,0,4,12,0,15,1,0,3,0,15,5,0,], #instruments/OPL/opl3_4op_piano_bell.fui
[485, 3, 0, 6,1,2,5,0,15,0,0,8,0,10,12,0,1,0,6,0,15,1,0,3,0,3,7,2,0,2,35,1,15,0,0,3,0,6,10,1,0,1,10,1,15,0,0,0,0,0,6,0,], #instruments/OPL/opl3_4op_lead_guitar.fui
[485, 3, 0, 7,1,5,34,0,15,0,0,2,0,9,15,1,1,0,18,0,15,1,0,0,0,0,15,0,0,2,28,1,4,0,0,3,0,15,15,2,0,0,10,1,7,0,0,0,0,15,6,0,], #instruments/OPL/opl3_4op_growl_synth.fui
[485, 3, 4, 7,1,8,0,0,15,0,0,5,0,10,0,0,1,2,15,0,15,0,0,9,0,15,6,7,0,2,6,0,15,0,0,6,0,10,10,0,0,1,0,0,15,0,0,0,0,0,0,0,], #instruments/OPL/opl3_4op_drum_tom.fui
[485, 3, 0, 7,1,4,0,0,15,0,0,3,0,4,0,0,1,2,0,0,15,0,0,10,0,9,6,0,0,2,8,0,15,0,0,6,0,7,10,0,0,1,0,0,15,0,0,0,0,0,0,0,], #instruments/OPL/opl3_4op_drum_snare.fui
[485, 3, 0, 3,1,3,0,0,15,0,1,3,0,2,7,2,1,1,0,0,15,0,0,8,0,1,15,2,0,2,0,0,15,0,0,4,0,8,4,6,0,0,4,0,15,0,0,5,0,15,9,6,], #instruments/OPL/opl3_4op_drum_power_snare.fui
[485, 3, 0, 7,1,4,0,0,15,0,0,5,0,8,0,0,1,2,0,0,15,0,0,11,0,13,6,0,0,2,8,0,15,0,0,8,0,11,10,0,0,1,0,0,15,0,0,0,0,0,0,0,], #instruments/OPL/opl3_4op_drum_kick.fui
[485, 3, 0, 0,1,5,32,0,2,1,0,3,0,11,4,0,1,3,18,0,9,1,0,1,0,15,5,0,0,0,23,0,15,1,0,4,0,5,4,0,0,1,0,0,15,0,0,5,0,1,5,0,], #instruments/OPL/OPL3-4opsitar.fui
[485, 3, 0, 0,1,9,25,0,5,0,0,2,0,5,10,0,1,1,26,0,15,0,0,2,0,6,7,0,0,6,41,0,12,0,0,1,0,7,7,0,0,1,0,0,6,0,0,2,0,1,13,0,], #instruments/OPL/OPL3-4opharmonica.fui
[485, 3, 0, 0,1,1,12,0,8,0,1,7,0,4,15,0,1,1,12,1,6,1,0,5,0,1,15,1,0,7,48,0,7,0,0,5,0,2,15,0,0,1,0,0,8,0,0,15,0,0,15,0,], #instruments/OPL/OPL3-4opbrass11.fui
[485, 3, 4, 4,0,2,22,1,15,0,0,0,0,0,0,0,0,2,22,0,15,1,2,0,0,0,0,0,0,2,10,1,7,0,0,6,0,1,6,0,0,2,10,0,7,1,1,6,0,1,4,0,], #instruments/OPL/opl3_4op_strings.fui
[485, 3, 4, 6,0,3,25,0,13,0,1,1,0,15,5,0,0,14,32,0,15,0,1,2,0,14,5,0,0,1,4,1,15,0,2,1,0,15,5,0,0,4,12,0,15,1,0,3,0,15,5,0,], #instruments/OPL/pianoBell.fui    
]

poblacio_inicial = []
for freq in parametres[0][4]:
    for bloc in parametres[1][4]:
        for instrument in poblacio_inicial_instruments_furnace:
            nou_instru = instrument.copy()
            nou_instru[0] = freq
            nou_instru[1] = bloc
            poblacio_inicial.append(nou_instru)

# Path to the WAV file 
solution_wav_file_path = "/home/jbd/Documents/JBD/furnace/BreakingGlass.wav"
#solution_wav_file_path = "/home/jbd/Documents/JBD/furnace/op4-strings_curt.wav"
solution_wav_sliced_file_path = '/home/jbd/Documents/JBD/furnace/solution_sliced.wav'

# Read the WAV file
sample_rate, solution_data = wavfile.read(solution_wav_file_path)
blocs_Wait = solution_data.shape[0] // 0xFFFF
Num_Samples_solution = solution_data.shape[0]
if sample_rate >= 44000:
    hop_length = 1024
else:
    hop_length = 512 # Aquest és el de defecte del librosa que mostreja a 22050
hop_length = 4096
# Display the sample rate and the shape of the data
print(f'Sample Rate: {sample_rate} Hz')
print(f'Data Shape: {solution_data.shape}')


# Havia pensat que podria fer un anàlisi cromàtic amb el librosa i analitzar el cronograma per saber la durada de cada bloc
# y,sr = librosa.load(solution_wav_file_path, sr=None) # Poso sr=None perquè agafi el sample_rate original, si no posa 22050
# chroma_cq = librosa.feature.chroma_cqt(y=y, sr=sr, hop_length=1024)
# fig, ax = plt.subplots(nrows=1, sharex=True, sharey=True)
# img = librosa.display.specshow(chroma_cq, y_axis='chroma', x_axis='time', sr=sr)
# fig.colorbar(img, ax=ax); plt.show()

# Però crec que senzillament faré blocs de 1024 perquè vagi analitzant la nota

def fitness_func(ga_instance, solution, solution_idx): 
    # Per provar he fet aquesta solució, però les comandes del 0x60 surten en 7 bits en lloc de 8. Són les solutions[8] i [11]
    #  solution = [ 647,4,3,6, 1, 7,59,1,6,0, 3,7,1,7,6, 3, 0, 6,49,1,4,0, 2,1,1,4,3, 2, 0, 4,39,1,6,1, 3,5,1,5,6, 4, 1, 3,29,1,4,0, 3,4,1,5,7, 4]

    # solution = [647,3,2,4,0, 2,22,1,15,0, 0,0,1,0,0, 0,0,2,22,0, 15,1,2,0,1, 0,0,0,0,2, 10,1,7,0,1, 6,1,1,6,0, 0,2,10,0,7, 1,2,6,1,1, 4,0] # La solució de 4op-strings

    solution = solution.astype(int)
    # print(f"{solution[3]} - {type(solution[3])}")
    comandes_hex = ''
    # Construïm les comandes a enviar
    # Configurem canal 0 com a 4op
    comandes_hex += '5F 05 03'
    comandes_hex += '5F 04 01'
    # Primer l'algoritme:
    if solution[2] == 0:
        comandes_hex += '5E C0' + format(0x30 | (solution[3]<<1),'02x')
        comandes_hex += '5E C3' + format(0x30 | (solution[3]<<1),'02x')
    elif solution[2] == 1:
        comandes_hex += '5E C0' + format(0x31 | (solution[3]<<1),'02x')
        comandes_hex += '5E C3' + format(0x30 | (solution[3]<<1),'02x')
    elif solution[2] == 2:
        comandes_hex += '5E C0' + format(0x30 | (solution[3]<<1),'02x')
        comandes_hex += '5E C3' + format(0x31 | (solution[3]<<1),'02x')
    elif solution[2] == 3:
        comandes_hex += '5E C0' + format(0x31 | (solution[3]<<1),'02x')
        comandes_hex += '5E C3' + format(0x31 | (solution[3]<<1),'02x')

    # Els registres 0x20
    comandes_hex += '5E 20' + format( (solution[9]<<7) | (solution[7]<<6) | (solution[12]<<5) | (solution[4]<<4) | solution[5], '02x')
    comandes_hex += '5E 28' + format( (solution[21]<<7) | (solution[19]<<6) | (solution[24]<<5) | (solution[16]<<4) | solution[17], '02x')
    comandes_hex += '5E 23' + format( (solution[33]<<7) | (solution[31]<<6) | (solution[36]<<5) | (solution[28]<<4) | solution[29], '02x')
    comandes_hex += '5E 2B' + format( (solution[45]<<7) | (solution[43]<<6) | (solution[48]<<5) | (solution[40]<<4) | solution[41], '02x')

    # Els registres 0x40
    comandes_hex += '5E 40' + format( (solution[10]<<6) | solution[6], '02x')
    comandes_hex += '5E 48' + format( (solution[22]<<6) | solution[18], '02x')
    comandes_hex += '5E 43' + format( (solution[34]<<6) | solution[30], '02x')
    comandes_hex += '5E 4B' + format( (solution[46]<<6) | solution[42], '02x')

    # Els registres 0x60
    comandes_hex += '5E 60' + format( (solution[8]<<4) | solution[11], '02x')
    comandes_hex += '5E 68' + format( (solution[20]<<4) | solution[23], '02x')
    comandes_hex += '5E 63' + format( (solution[32]<<4) | solution[35], '02x')
    comandes_hex += '5E 6B' + format( (solution[44]<<4) | solution[47], '02x')

    # Els registre 0x80
    comandes_hex += '5E 80' + format( (solution[13]<<4) | solution[14], '02x')
    comandes_hex += '5E 88' + format( (solution[25]<<4) | solution[26], '02x')
    comandes_hex += '5E 83' + format( (solution[37]<<4) | solution[38], '02x')
    comandes_hex += '5E 8B' + format( (solution[49]<<4) | solution[50], '02x')

    # Els registre 0xE0
    comandes_hex += '5E E0' + format( (solution[15]), '02x')
    comandes_hex += '5E E8' + format( (solution[27]), '02x')
    comandes_hex += '5E E3' + format( (solution[39]), '02x')
    comandes_hex += '5E EB' + format( (solution[51]), '02x')

    # Configurem que no té drums
    comandes_hex += '5E BD 00'

    # Configurem la nota a sonar
    comandes_hex += '5E A0' + format(solution[0] & 0x00FF, '02x')
    comandes_hex += '5E B0' + format( 0x20 | (solution[1]<<2) | (solution[0]>>8), '02x' )

    # Ara hem de fer les pauses per agafar totes les mostres que sonin quan es converteix a wav
    # Mirem les mostres que hi ha a la wav ideal
    if Num_Samples_processats + hop_length > Num_Samples_solution:
        samples_num = (Num_Samples_solution - Num_Samples_processats) & 0xffff
        samples = format( samples_num, '04x')
    else:
        samples_num = hop_length & 0xffff
        samples = format(hop_length , '04x')
    comandes_hex += '61 ' + samples[2:4] + samples[0:2] # en little endian

    # Apaguem la nota i tanquem el wav file
    comandes_hex += '5E B0 00 66'

    # Hem de posar bé el EOF en la posició 4 del fitxer i posar 4 00 al final?
    with open('/home/jbd/Documents/JBD/furnace/fitx_heading_OPL4.vgm', 'rb') as file:
        file_content = file.read()

    #pdb.set_trace()
    nou_content = bytearray(file_content + bytes.fromhex(comandes_hex))
    eof = len(nou_content).to_bytes(4, byteorder='little')
    nou_content[4:8] = eof
    nou_content += bytes([0,0,0,0])

    # total samples
    Num_Samp = samples_num.to_bytes(4, byteorder='little')
    nou_content[0x18:0x1C] = Num_Samp
    # Loop offset
    nou_content[0x1c:0x20] = int(0).to_bytes(4)
    nou_content[0x20:0x24] = int(0).to_bytes(4)

    with open(f'/home/jbd/Documents/JBD/furnace/borram_{solution_idx}.vgm', 'wb') as file:
        file.write(nou_content)

    # Ara generem el nou wav. Utilitzarem el solution_idx per assegurar que no es matxaquen els uns amb els altres
    subprocess.run(["/home/jbd/Documents/JBD/vgm/vgmplay-legacy/VGMPlay/vgm2wav",f"/home/jbd/Documents/JBD/furnace/borram_{solution_idx}.vgm",f"/home/jbd/Documents/JBD/furnace/borram_{solution_idx}.wav"])
    # La informació està pels dos canals, però jo la tinc mono
    audios = AudioSimilarity(f"/home/jbd/Documents/JBD/furnace/borram_{solution_idx}.wav", solution_wav_sliced_file_path, sample_rate=sample_rate, verbose=False)

    # En poques mostres, sembla que hi ha operacions que no les calcula bé o dona nan
    fitness = np.nan_to_num(audios.zcr_similarity()) + np.nan_to_num(audios.rhythm_similarity()) + np.nan_to_num(audios.chroma_similarity())
    try: 
        fitness += np.nan_to_num(audios.spectral_contrast_similarity())
    except Exception:
        pass
    try:
        fitness += np.nan_to_num(audios.perceptual_similarity())
    except Exception:
        pass
    try:
        fitness += np.nan_to_num(audios.stent_weighted_audio_similarity())
    except Exception:
        pass

    # Hauria de donar més pesos als que siguin més importants
    # print(f"{fitness} - {type(fitness)}")
    if np.isnan(fitness):
        fitness = 0
    return fitness


def on_generation_jep(ga_instance):
    global start_time
    end_time = time.time()
    elapsed_time = end_time - start_time

    print(f"Generation = {ga_instance.generations_completed}")
    print(f"Fitness of the best solution = {ga_instance.best_solution()[1]}")
    print(f"Best solution index: {ga_instance.best_solution()}")
    print(f"Average Fitness = {np.mean(ga_instance.last_generation_fitness)}")
    print(f"Diversity = {len(np.unique(ga_instance.population, axis=0))}")
    print(f"Time passed in this generation = {elapsed_time:.4f} seconds")
    start_time = time.time()

    # Save the state every 20 generations
    if ga_instance.generations_completed % 20 == 0:
        save_state(ga_instance, f'ga_instance_state_{ga_instance.generations_completed}.pkl')
    

def save_state(ga_instance, filename):
    with open(filename, 'wb') as file:
        pickle.dump(ga_instance, file)
        print(f"State saved at generation {ga_instance.generations_completed}")

def load_state(filename):
    with open(filename, 'rb') as file:
        ga_instance = pickle.load(file)
        print(f"State loaded from generation {ga_instance.generations_completed}")
        return ga_instance

# inicialitzem el time
start_time = time.time()

# Check if there is a saved state to load
if os.path.exists('ga_instance_state_21.pkl'):
    print("*** Carreguem l'estat que ja teníem")
    ga_instance = load_state('ga_instance_state_20.pkl')
else:
    ga_instance = pygad.GA(
        num_generations = 10,
        num_parents_mating = int(len(poblacio_inicial)/2),
        initial_population = poblacio_inicial,
        num_genes = len(espai_gens), # són el nombre de paràmetres
        mutation_type = "adaptive",
        mutation_probability = [0.4,0.1], # Un 40% que mutin dels que no tenen bons gens i un 10% dels gens muten de la nova generació
        fitness_func = fitness_func,
        stop_criteria = ["reach_5.60", "saturate_5"], # Hi ha saturació o reach, saturació és que es repeteixi i reach que s'acosti a l'òptim, però es poden combinar els dos
        gene_space = espai_gens, #he de donar el valor dels paràlmetres que poden prendre
        on_generation = on_generation_jep,
        parent_selection_type = "tournament",
        keep_elitism = int(len(poblacio_inicial)/4) # Guardem una proporció dels millors
    )

solucions_optimes_hop = []
Num_Samples_processats = 0

while Num_Samples_processats < Num_Samples_solution:
    sr, data = wavfile.read(solution_wav_file_path)
    sliced_data = data[Num_Samples_processats:Num_Samples_processats + hop_length]
    wavfile.write(solution_wav_sliced_file_path, sr, sliced_data)

    try:
        ga_instance.run()
    except Exception as e:
        # Peta perquè hi ha un nan en els fitness
        print(f"IndexError encountered: {e}")
        print("Checking the population fitness array and other parameters.")
        pop_fitness = ga_instance.last_generation_fitness
        print("Population Fitness: ", pop_fitness)
        if len(pop_fitness) == 0:
            raise ValueError("Population fitness array is empty. Check the fitness function and initial population.")


    best_solution, best_solution_fitness, best_match_idx = ga_instance.best_solution()
    print(f"Trobada {best_solution} - {best_solution_fitness} - TLs: {best_solution[6]}, {best_solution[18]}, {best_solution[30]}, {best_solution[42]}")
    solucions_optimes_hop.append(best_solution)
    fitness_func(ga_instance, best_solution, f'best_{Num_Samples_processats}')
    Num_Samples_processats += hop_length

with open('solucions_optimes.pkl', 'wb') as file:
    pickle.dump(solucions_optimes_hop, file)
    print("\U0001F389\U0001F389\U0001F389 Hem acabat")

# Ara agafo aquesta solució òptima i hi poso totes les notes per fer la nova població inicial
# Aquesta segona iteració no ha funcionat, fins i tot ha donat un resultat pitjor.
# Ha començat aquesta segona amb l'òptima anterior:
# Trobada [432.   5.   2.   3.   1.   5.  42.   0.   0.   0.   0.   1.   0.   2.
#    7.   3.   1.   0.  43.   1.   2.   1.   0.  11.   1.  14.  11.   5.
#    0.  14.  53.   1.  11.   1.   3.   9.   0.   6.   3.   2.   1.  12.
#   23.   0.  12.   1.   0.   1.   1.  13.  10.   5.] - 4.99537423037037 - TLs: 42.0, 43.0, 53.0, 23.0
# I ha acabat amb aquest que és pitjor que amb el que ha començat:
# Best solution index: (array([611.,   7.,   0.,   6.,   1.,   5.,  26.,   1.,   0.,   0.,   0.,
#          9.,   1.,   4.,  13.,   5.,   1.,   7.,  43.,   0.,   2.,   0.,
#          0.,   7.,   1.,  14.,  11.,   5.,   1.,   2.,   9.,   0.,  10.,
#          1.,   3.,   9.,   1.,   6.,   5.,   2.,   0.,  15.,  25.,   0.,
#          9.,   1.,   3.,   3.,   0.,  10.,   1.,   0.]), 4.741897884463173, 0)
# Average Fitness = 3.7381810743688875


# Si vull agafar les dades he de canviar la funció:
# # Callback function to collect data during the run
# generation_data = []

# def on_generation(ga_instance):
#     generation_data.append({
#         "generation": ga_instance.generations_completed,
#         "best_fitness": ga_instance.best_solution()[1],
#         "average_fitness": np.mean(ga_instance.last_generation_fitness),
#         "diversity": len(np.unique(ga_instance.population, axis=0))
#     })
#     print(f"Generation = {ga_instance.generations_completed}")
#     print(f"Best Fitness = {ga_instance.best_solution()[1]}")
#     print(f"Average Fitness = {np.mean(ga_instance.last_generation_fitness)}")
#     print(f"Diversity = {len(np.unique(ga_instance.population, axis=0))}")

#     # Save the state every 20 generations
#     if ga_instance.generations_completed % 20 == 0:
#         save_state(ga_instance, 'ga_instance_state.pkl')
# import matplotlib.pyplot as plt

# generations = [data["generation"] for data in generation_data]
# best_fitness = [data["best_fitness"] for data in generation_data]
# average_fitness = [data["average_fitness"] for data in generation_data]
# diversity = [data["diversity"] for data in generation_data]

# plt.figure(figsize=(12, 6))

# plt.subplot(3, 1, 1)
# plt.plot(generations, best_fitness, label="Best Fitness", color="blue")
# plt.xlabel("Generation")
# plt.ylabel("Fitness")
# plt.title("Best Fitness over Generations")
# plt.legend()

# plt.subplot(3, 1, 2)
# plt.plot(generations, average_fitness, label="Average Fitness", color="green")
# plt.xlabel("Generation")
# plt.ylabel("Fitness")
# plt.title("Average Fitness over Generations")
# plt.legend()

# plt.subplot(3, 1, 3)
# plt.plot(generations, diversity, label="Diversity", color="red")
# plt.xlabel("Generation")
# plt.ylabel("Number of Unique Solutions")
# plt.title("Population Diversity over Generations")
# plt.legend()

# plt.tight_layout()
# plt.show()

# When dealing with a large search space, such as 52 genes and 10141014 possible combinations, setting appropriate initial parameters for a genetic algorithm (GA) is crucial for effective optimization. Here are some initial considerations and recommendations for setting up PyGAD:
# Key Parameters to Consider

#     Population Size (sol_per_pop): A larger population size can provide more diversity, which is crucial for exploring a large search space. However, it will also require more computational resources.
#     Number of Generations (num_generations): This determines how long the algorithm will run. More generations allow for more thorough exploration and optimization.
#     Number of Parents Mating (num_parents_mating): Determines how many parents are selected for producing offspring. A higher number increases diversity but also computational complexity.
#     Mutation Probability (mutation_probability): This introduces variations into the population. A higher mutation rate can prevent premature convergence but may also disrupt convergence if too high.

# Initial Parameters

# Here is a suggested setup for the initial parameters:

#     Population Size: 1000 (this is a starting point; you may need to adjust based on available computational resources and the problem's complexity).
#     Number of Generations: 1000 (again, this is a starting point; you may need to run experiments to find the optimal number).
#     Number of Parents Mating: 500 (50% of the population; this can be adjusted based on the specific needs of your problem).
#     Mutation Probability: 0.1 (10%; this is a typical starting value, but you may need to experiment to find the best rate for your problem).
